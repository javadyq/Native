/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * encTurbo is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 *     http://license.coscl.org.cn/MulanPSL
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

#include "va_encode_impl.h"

#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <va_drmcommon.h>

#include "va_display_drm.h"

/**
 * dump中间数据宏定义
 */
#define DUMP_FILE_PATH          "/data/."
#define DUMP_RGB_FILE_TAG       "/data/dumprgb"
#define DUMP_YUV_FILE_TAG       "/data/dumpyuv"
#define DUMP_H264_FILE_TAG      "/data/dumph264"
#define DUMP_TIMEUSE_FILE_TAG   "/data/dumptime"

/**
 * dump中间数据全局变量定义
 */
FILE *g_dumpYuvFp = NULL;
FILE *g_dumpRgbFp = NULL;
FILE *g_dumpH264Fp = NULL;
FILE *g_dumpTimeuseFp = NULL;

// create video buffer only support the follow format when vaCreateSurfaces
static struct {
    const char* fmt_str;
    int fourcc;
} g_fourccMap[] = {
    {"YV12", VA_FOURCC_YV12}, // 4:2:0 VVVV UUUU
    {"NV12", VA_FOURCC_NV12}, // 4:2:0 UV UV UV UV
    {"YUYV", VA_FOURCC_YUY2}, // 4:2:2 YUYV YUYV
    {"UYVY", VA_FOURCC_UYVY}, // 4:2:2.UYUV UYUV
    {"UYVY", VA_FOURCC_P016}, // 4:2:0-16bit
};

// H.264 table A-1.
typedef struct {
    const char *name;
    unsigned char level_idc;
    unsigned char constraint_set3_flag;
    unsigned int max_mbps;
    unsigned int max_fs;
    unsigned int max_dpb_mbs;
    unsigned int max_br;
    unsigned int max_cpb;
    unsigned short max_v_mv_r;
    unsigned char min_cr;
    unsigned char max_mvs_per_2mb;
} H264Level;

/**
 * 功能说明：编码level建模
 */
static H264Level g_h264Levels[] = {
    // Name          MaxMBPS                   MaxBR              MinCR
    //  | level_idc     |       MaxFS            |    MaxCPB        | MaxMvsPer2Mb
    //  |     | cs3f    |         |  MaxDpbMbs   |       |  MaxVmvR |   |
    { "1",   10, 0,     1485,     99,    396,     64,    175,   64, 2,  0 },
    { "1b",  11, 1,     1485,     99,    396,    128,    350,   64, 2,  0 },
    { "1b",   9, 0,     1485,     99,    396,    128,    350,   64, 2,  0 },
    { "1.1", 11, 0,     3000,    396,    900,    192,    500,  128, 2,  0 },
    { "1.2", 12, 0,     6000,    396,   2376,    384,   1000,  128, 2,  0 },
    { "1.3", 13, 0,    11880,    396,   2376,    768,   2000,  128, 2,  0 },
    { "2",   20, 0,    11880,    396,   2376,   2000,   2000,  128, 2,  0 },
    { "2.1", 21, 0,    19800,    792,   4752,   4000,   4000,  256, 2,  0 },
    { "2.2", 22, 0,    20250,   1620,   8100,   4000,   4000,  256, 2,  0 },
    { "3",   30, 0,    40500,   1620,   8100,  10000,  10000,  256, 2, 32 },
    { "3.1", 31, 0,   108000,   3600,  18000,  14000,  14000,  512, 4, 16 },
    { "3.2", 32, 0,   216000,   5120,  20480,  20000,  20000,  512, 4, 16 },
    { "4",   40, 0,   245760,   8192,  32768,  20000,  25000,  512, 4, 16 },
    { "4.1", 41, 0,   245760,   8192,  32768,  50000,  62500,  512, 2, 16 },
    { "4.2", 42, 0,   522240,   8704,  34816,  50000,  62500,  512, 2, 16 },
    { "5",   50, 0,   589824,  22080, 110400, 135000, 135000,  512, 2, 16 },
    { "5.1", 51, 0,   983040,  36864, 184320, 240000, 240000,  512, 2, 16 },
    { "5.2", 52, 0,  2073600,  36864, 184320, 240000, 240000,  512, 2, 16 },
    { "6",   60, 0,  4177920, 139264, 696320, 240000, 240000, 8192, 2, 16 },
    { "6.1", 61, 0,  8355840, 139264, 696320, 480000, 480000, 8192, 2, 16 },
    { "6.2", 62, 0, 16711680, 139264, 696320, 800000, 800000, 8192, 2, 16 },
};

/**
 * 功能说明：以frame_idx标志按照升序或者降序进行一趟快速排序，
 *           升序：排序后达到比key小的值都在key的左侧，比key大的值都在key的右侧
 *           降序：排序后达到比key小的值都在key的右侧，比key大的值都在key的左侧
 * 输入参数：ref 待排序的序列
 *           key 排序参考关键key
 *           ascending 是否升序
 */
#define PARTITION_BY_FRAMEIDX(ref, key, ascending)      \
    while (i <= j) {                                            \
        if (ascending) {                                        \
            while ((ref)[i].frame_idx < (key))                  \
                i++;                                            \
            while ((ref)[j].frame_idx > (key))                  \
                j--;                                            \
        } else {                                                \
            while ((ref)[i].frame_idx > (key))                  \
                i++;                                            \
            while ((ref)[j].frame_idx < (key))                  \
                j--;                                            \
        }                                                       \
        if (i <= j) {                                           \
            tmp = (ref)[i];                                     \
            (ref)[i] = (ref)[j];                                \
            (ref)[j] = tmp;                                     \
            i++;                                                \
            j--;                                                \
        }                                                       \
    }                                                           \

/**
 * 功能说明：以TopFieldOrderCnt标志按照升序或者降序进行一趟快速排序，
 *           升序：排序后达到比key小的值都在key的左侧，比key大的值都在key的右侧
 *           降序：排序后达到比key小的值都在key的右侧，比key大的值都在key的左侧
 * 输入参数：ref 待排序的序列
 *           key 排序参考关键key
 *           ascending 是否升序
 */
#define PARTITION_BY_TOPFIELD(ref, key, ascending)      \
    while (i <= j) {                                            \
        if (ascending) {                                        \
            while ((ref)[i].TopFieldOrderCnt < (key))           \
                i++;                                            \
            while ((ref)[j].TopFieldOrderCnt > (key))           \
                j--;                                            \
        } else {                                                \
            while ((ref)[i].TopFieldOrderCnt > (key))           \
                i++;                                            \
            while ((ref)[j].TopFieldOrderCnt < (key))           \
                j--;                                            \
        }                                                       \
        if (i <= j) {                                           \
            tmp = (ref)[i];                                     \
            (ref)[i] = (ref)[j];                                \
            (ref)[j] = tmp;                                     \
            i++;                                                \
            j--;                                                \
        }                                                       \
    }                                                           \

/**
 * 功能说明：释放打开的文件资源
 */
static void DumpResourceFree()
{
    ENC_INF_PRT("\n");

    if (g_dumpRgbFp) {
        fclose(g_dumpRgbFp);
        g_dumpRgbFp = NULL;
    }
    if (g_dumpYuvFp) {
        fclose(g_dumpYuvFp);
        g_dumpYuvFp = NULL;
    }
    if (g_dumpTimeuseFp) {
        fclose(g_dumpTimeuseFp);
        g_dumpTimeuseFp = NULL;
    }
    if (g_dumpH264Fp) {
        fclose(g_dumpH264Fp);
        g_dumpH264Fp = NULL;
    }
    return;
}

/**
 * 功能说明：根据设备上的标签判断是否保存rgb数据到本地文件
 *           每编码一帧时更新一次
 */
static bool IsDumpRGB()
{
    if (access(DUMP_RGB_FILE_TAG, F_OK) == 0) {
        if (!g_dumpRgbFp) {
            time_t rawTime;
            time(&rawTime);
            struct tm *local = localtime(&rawTime);
            char storeFile[MAX_ARRAY_SIZE];
            snprintf(storeFile, MAX_ARRAY_SIZE - 1, "%s/dump_rgb_%d_%04d%02d%02d_%02d%02d%02d.rgb", DUMP_FILE_PATH,
                     getpid(),
                     local->tm_year, local->tm_mon, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
            g_dumpRgbFp = fopen(storeFile, "wb+");
        }
    } else {
        if (g_dumpRgbFp) {
            fclose(g_dumpRgbFp);
            g_dumpRgbFp = NULL;
        }
    }
    if (!g_dumpRgbFp) {
        return false;
    }
    return true;
}

/**
 * 功能说明：根据设备上的标签判断是否保存yuv数据到本地文件
 *           每编码一帧时更新一次
 */
static bool IsDumpYUV()
{
    if (access(DUMP_YUV_FILE_TAG, F_OK) == 0) {
        if (!g_dumpYuvFp) {
            time_t rawTime;
            time(&rawTime);
            struct tm *local = localtime(&rawTime);
            char storeFile[MAX_ARRAY_SIZE];
            snprintf(storeFile, MAX_ARRAY_SIZE - 1, "%s/dump_yuv_%d_%04d%02d%02d_%02d%02d%02d.yuv", DUMP_FILE_PATH,
                     getpid(),
                     local->tm_year, local->tm_mon, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
            g_dumpYuvFp = fopen(storeFile, "wb+");
        }
    } else {
        if (g_dumpYuvFp) {
            fclose(g_dumpYuvFp);
            g_dumpYuvFp = NULL;
        }
    }

    if (!g_dumpYuvFp) {
        return false;
    }
    return true;
}

/**
 * 功能说明：根据设备上的标签判断是否保存h264数据到本地文件
 *           每编码一帧时更新一次
 */
static bool IsDumpH264()
{
    if (access(DUMP_H264_FILE_TAG, F_OK) == 0) {
        if (!g_dumpH264Fp) {
            time_t rawTime;
            time(&rawTime);
            struct tm *local = localtime(&rawTime);
            char storeFile[MAX_ARRAY_SIZE];
            snprintf(storeFile, MAX_ARRAY_SIZE - 1, "%s/dump_h264_%d_%04d%02d%02d_%02d%02d%02d.h264", DUMP_FILE_PATH,
                     getpid(),
                     local->tm_year, local->tm_mon, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
            g_dumpH264Fp = fopen(storeFile, "wb+");
        }
    } else {
        if (g_dumpH264Fp) {
            fclose(g_dumpH264Fp);
            g_dumpH264Fp = NULL;
        }
    }

    if (!g_dumpH264Fp) {
        return false;
    }
    return true;
}

/**
 * 功能说明：根据设备上的标签判断是否保存时延到本地文件
 *           每编码一帧时更新一次
 */
static bool IsDumpTimeuse()
{
    if (access(DUMP_TIMEUSE_FILE_TAG, F_OK) == 0) {
        if (!g_dumpTimeuseFp) {
            time_t rawTime;
            time(&rawTime);
            struct tm *local = localtime(&rawTime);
            char storeFile[MAX_ARRAY_SIZE];
            snprintf(storeFile, MAX_ARRAY_SIZE - 1, "%s/dump_timeuse_%d_%04d%02d%02d_%02d%02d%02d.txt", DUMP_FILE_PATH,
                     getpid(),
                     local->tm_year, local->tm_mon, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
            g_dumpTimeuseFp = fopen(storeFile, "wb+");
        }
    } else {
        if (g_dumpTimeuseFp) {
            fclose(g_dumpTimeuseFp);
            g_dumpTimeuseFp = NULL;
        }
    }

    if (!g_dumpTimeuseFp) {
        return false;
    }
    return true;
}

/**
 * 功能说明：保存RGB数据帧到本地文件
 */
static ENCStatus DumpRGBData(avcenc_context_s *encCtx)
{
    if (!IsDumpRGB()) {
        return ENC_STATUS_SUCCESS;
    }
    VAStatus vaStatus;
    VAImage outImage;
    VAImageFormat imageFormat;
    memset(&imageFormat, 0, sizeof(imageFormat));
    imageFormat.fourcc = encCtx->vpp.pixel_format; // RGB数据格式：VA_FOURCC_BGRA

    // 创建一块内存空间
    vaStatus = vaCreateImage(encCtx->va_dpy, &imageFormat, encCtx->vpp.width, encCtx->vpp.height, &outImage);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateImage", ENC_STATUS_ERROR_VA_STATUS);

    // 把RGB数据从显卡空间拷贝到内存空间
    vaStatus = vaGetImage(encCtx->va_dpy, encCtx->vpp.surface_id, 0, 0, 
                          encCtx->vpp.width, encCtx->vpp.height, outImage.image_id);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaGetImage", ENC_STATUS_ERROR_VA_STATUS);

    // 把已拷贝到内存空间的数据映射到用户数据
    void *pBuf = NULL;
    vaStatus = vaMapBuffer(encCtx->va_dpy, outImage.buf, &pBuf);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaMapBuffer", ENC_STATUS_ERROR_VA_STATUS);

    if (g_dumpRgbFp) {
        fwrite(pBuf, 1, encCtx->vpp.width * encCtx->vpp.height * RGB_BYTE_ONE_PIXEL, g_dumpRgbFp);
        fflush(g_dumpRgbFp);
    }

    // 释放资源
    vaStatus = vaUnmapBuffer(encCtx->va_dpy, outImage.buf);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaUnmapBuffer", ENC_STATUS_ERROR_VA_STATUS);

    vaStatus = vaDestroyImage(encCtx->va_dpy, outImage.image_id);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaDestroyImage", ENC_STATUS_ERROR_VA_STATUS);

    return  ENC_STATUS_SUCCESS;
}

/**
 * 保存yuv数据帧到本地文件
 * 输入参数：void *buf 输出的数据指针
 *           int size 输出的数据大小
 */
static void DumpYUVData(void *buf, int size)
{
    if (!IsDumpYUV() || !g_dumpYuvFp) {
        return;
    }

    fwrite(buf, 1, size, g_dumpYuvFp);
    fflush(g_dumpYuvFp);
    return;
}

/**
 * 功能说明：保存编码后的h264帧到本地文件
 * 输入参数：void *buf 输出的数据指针
 *           int size 输出的数据大小
 */
static void DumpH264Data(void *buf, int size)
{
    if (!IsDumpH264() || !g_dumpH264Fp) {
        return;
    }

    fwrite(buf, 1, size, g_dumpH264Fp);
    fflush(g_dumpH264Fp);
    return;
}

/**
 * 功能说明：获取系统时钟值
 */
static struct timeval GetTimeOfDay()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv;
}

/**
 * 功能说明：计算每个阶段的时延，保存时延到本地文件，并决定是否打印时延值到终端
 * 输入参数：struct timeval start，开始时间
 *           const char *message 输出的消息
 *           bool print 是否输出到终端
 *           int frameIndex 判断输出的消息携带帧序列号
 */
static void DumpTimeuse(struct timeval start, const char *message, bool print, int frameIndex)
{
    if (!IsDumpTimeuse() || !g_dumpTimeuseFp) {
        return;
    }
    int usecPerSecond = 1000000;
    int msecPerSecond = 1000;
    struct timeval end;
    long long timeDiff;
    double timeuse;

    // 获取当前时间
    end = GetTimeOfDay();
    timeDiff = (usecPerSecond * (end.tv_sec - start.tv_sec) + end.tv_usec - start.tv_usec);
    timeuse = (double)timeDiff / msecPerSecond;

    char timeBuf[MAX_ARRAY_SIZE];
    if (frameIndex >= 0) {
        snprintf(timeBuf, MAX_ARRAY_SIZE - 1, "%s(ms):%f\n%d\n\n", message, timeuse, frameIndex);
    } else {
        snprintf(timeBuf, MAX_ARRAY_SIZE - 1, "%s(ms):%f\n", message, timeuse);
    }
    if (print) {
        printf("%s", timeBuf);
    }

    // 写到本地文件
    fwrite(timeBuf, strlen(timeBuf), 1, g_dumpTimeuseFp);
    fflush(g_dumpTimeuseFp);
    return;
}

/**
 * 功能说明：根据fourcc值转换字符串用于打印
 */
static const char* MapFourccToStr (int fourcc)
{
    for (unsigned int i = 0; i < sizeof(g_fourccMap) / sizeof(g_fourccMap[0]); i++) {
        if (fourcc == g_fourccMap[i].fourcc) {
            return g_fourccMap[i].fmt_str;
        }
    }
    return "unkown format";
}

/**
 * 功能说明：查找free slot idx
 */
static int UtilityGetFreeSlot(avcenc_context_s *encCtx)
{
    int i;
    int idx = -1;

    for (i = 0; i < SURFACE_NUM; i++) {
        if (encCtx->enc.use_slot[i] == 0) {
            idx = i;
            break;
        }
    }
    if (idx < 0) {
        ENC_ERR_PRT("WARNING: No free slot to store the reconstructed frame \n");
        idx = SURFACE_NUM - 1;
    }
    return idx;
}

/**
 * 功能说明：排序，对ref序列按照升序或者降序排序，采用快速排序法
 */
static void SortOne(VAPictureH264 ref[], int left, int right, int ascending, int frameIdx)
{
    int i = left;
    int j = right;
    unsigned int key;
    VAPictureH264 tmp;
    int idx = (left + right) >> 1;
    if (frameIdx) {
        key = ref[idx].frame_idx;
        PARTITION_BY_FRAMEIDX(ref, key, ascending); // 按照frameIdx排序
    } else {
        key = ref[idx].TopFieldOrderCnt;
        PARTITION_BY_TOPFIELD(ref, (signed int)key, ascending); // 按照TopFieldOrderCnt排序
    }

    if (left < j) { // 递归
        SortOne(ref, left, j, ascending, frameIdx);
    }

    if (i < right) { // 递归
        SortOne(ref, i, right, ascending, frameIdx);
    }
}

/**
 * 功能说明：排序，对ref序列按照升序或者降序排序，采用快速排序法
 */
static void SortTwo(VAPictureH264 ref[], int left, int right, unsigned int key, unsigned int frameIdx,
                    int partitionAscending, int list0Ascending, int list1Ascending)
{
    int i = left;
    int j = right;
    VAPictureH264 tmp;

    if (frameIdx) {
        PARTITION_BY_FRAMEIDX(ref, key, partitionAscending); // 按照frameIdx排序
    } else {
        PARTITION_BY_TOPFIELD(ref, (signed int)key, partitionAscending); // 按照TopFieldOrderCnt排序
    }
    
    SortOne(ref, left, i - 1, list0Ascending, frameIdx); // 递归，继续查找key左侧的序列
    SortOne(ref, j + 1, right, list1Ascending, frameIdx); // 递归，继续查找key右侧的序列

    return;
}

/**
 * 功能说明：根据profileidc/framerate等计算编码级别
 */
static H264Level *GuessLevel(int profileIdc, int64_t bitrate, int framerate,
    int width, int height, int maxDecFrameBuffering)
{
    // we can not support HIGH
    if (profileIdc == PROFILE_IDC_HIGH) {
        return NULL;
    }

    int bitsPerByte = 8;
    int maxDpbFramesValue = 16;
    unsigned int mbsWidth  = (width + SIZE_ALIGN - 1) / SIZE_ALIGN;
    unsigned int mbsHeight = (height + SIZE_ALIGN - 1) / SIZE_ALIGN;

    unsigned int items = sizeof(g_h264Levels) / sizeof(g_h264Levels[0]);
    int br = CPB_BR_NAL_FACTOR_BASE; // br = CPB_BR_NAL_FACTOR_HIGH if profileIdc == PROFILE_IDC_MAIN

    for (unsigned i = 0; i < items; i++) {
        H264Level *level = &g_h264Levels[i];

        if (bitrate > (int64_t)level->max_br * br ||
            mbsWidth * mbsHeight > level->max_fs ||
            mbsWidth * mbsWidth  > bitsPerByte * level->max_fs ||
            mbsHeight * mbsHeight > bitsPerByte * level->max_fs) {
            continue;
        }

        int maxDpbFramesTmp = level->max_dpb_mbs / (mbsWidth * mbsHeight);
        int maxDpbFrames = (maxDpbFramesTmp > maxDpbFramesValue) ? maxDpbFramesValue : maxDpbFramesTmp;
        int fr = (int)(level->max_mbps / (mbsWidth * mbsHeight));
        if (maxDecFrameBuffering > maxDpbFrames || framerate > fr) {
            continue;
        }

        return level;
    }

    return NULL;
}

/**
 * 功能说明：SPS table参数初始化
 */
static void EncContextSeqParamInit(avcenc_context_s *encCtx)
{
    if (!encCtx) {
        return;
    }

    VAEncSequenceParameterBufferH264 *seqParam = &(encCtx->enc.seq_param);
    unsigned int mbWidth = (encCtx->enc.picture_width + SIZE_ALIGN - 1) / SIZE_ALIGN;
    unsigned int mbHeight = (encCtx->enc.picture_height + SIZE_ALIGN - 1) / SIZE_ALIGN;
    int maxFramesB = 0; // b frames, encTurbo donot need encode b 
    
    int countingType = 2; // n_frames drop method, if counting type =2 ,drop the only zero value in n_frames counter
    int levelIdcDefaultValue = 32;

    // 根据bit rate / frame rate计算出level
    const H264Level *level = GuessLevel(encCtx->enc.profile_idc, encCtx->enc.bit_rate, encCtx->enc.frame_rate,
                                        mbWidth * SIZE_ALIGN, mbHeight * SIZE_ALIGN, maxFramesB + 1);
    seqParam->seq_parameter_set_id = 0;
    seqParam->level_idc = (level != NULL) ? level->level_idc : levelIdcDefaultValue;
    seqParam->intra_period = encCtx->enc.intra_period; // I帧
    seqParam->intra_idr_period = seqParam->intra_period; // IDR帧
    seqParam->ip_period = 1 + maxFramesB; // 1;this is mean how many b frames per P，此处默认B帧为0
    seqParam->bits_per_second = (encCtx->enc.bit_rate > 0) ? encCtx->enc.bit_rate : 0;
    seqParam->max_num_ref_frames = (seqParam->intra_period == 1) ? 0 : (maxFramesB + 1);
    seqParam->picture_width_in_mbs = mbWidth;
    seqParam->picture_height_in_mbs = mbHeight;
    seqParam->seq_fields.bits.chroma_format_idc = 1;
    seqParam->seq_fields.bits.frame_mbs_only_flag = 1;
    seqParam->seq_fields.bits.direct_8x8_inference_flag = 1;
    seqParam->seq_fields.bits.log2_max_frame_num_minus4 = LOG2_MAX_FRAME_NUM;
    seqParam->seq_fields.bits.log2_max_pic_order_cnt_lsb_minus4 = LOG2_MAX_PIC_ORDER_CNT_LSB;
    seqParam->seq_fields.bits.pic_order_cnt_type = 0; // 影响第一帧出帧的速度

    if (encCtx->enc.picture_width != SIZE_ALIGN * mbWidth || encCtx->enc.picture_height != SIZE_ALIGN * mbHeight) {
        /** @name Cropping (optional) */
        seqParam->frame_cropping_flag = 1;
        seqParam->frame_crop_left_offset = 0;
        seqParam->frame_crop_right_offset = (SIZE_ALIGN * mbWidth - encCtx->enc.picture_width) >> 1;
        seqParam->frame_crop_top_offset = 0;
        seqParam->frame_crop_bottom_offset = (SIZE_ALIGN * mbHeight - encCtx->enc.picture_height) >> 1;
    }
    if (encCtx->enc.bit_rate > 0) {
        /** @name VUI parameters (optional) */
        int log2MaxMvLength = 15;
        seqParam->vui_parameters_present_flag = 1; // HRD info located in vui
        seqParam->vui_fields.bits.aspect_ratio_info_present_flag = 1;
        seqParam->vui_fields.bits.timing_info_present_flag = 1;
        seqParam->vui_fields.bits.bitstream_restriction_flag = 1;
        seqParam->vui_fields.bits.log2_max_mv_length_horizontal = log2MaxMvLength;
        seqParam->vui_fields.bits.log2_max_mv_length_vertical = log2MaxMvLength;
        seqParam->vui_fields.bits.fixed_frame_rate_flag = 1;
        seqParam->aspect_ratio_idc = 1;
        seqParam->num_units_in_tick = 1; // is num_units_in_tick / time_sacle 
        seqParam->time_scale = encCtx->enc.frame_rate * countingType;
    }
    return;
}

/**
 * 功能说明：PPS table参数初始化
 */
static void EncContextPicParamInit(avcenc_context_s *encCtx)
{
    if (!encCtx) {
        ENC_ERR_PRT("context null !!!\n");
        return;
    }

    int baselineOrMainlineMask = 0x7;
    VAEncPictureParameterBufferH264 *picParam = &(encCtx->enc.pic_param);
    
    picParam->CurrPic.picture_id = VA_INVALID_ID;
    picParam->CurrPic.flags = VA_PICTURE_H264_INVALID;
    picParam->CurrPic.TopFieldOrderCnt = 0;
    picParam->CurrPic.BottomFieldOrderCnt = 0;

    picParam->coded_buf = VA_INVALID_ID;
    picParam->pic_parameter_set_id = 0;
    picParam->seq_parameter_set_id = 0;
    picParam->last_picture = 0;
    picParam->frame_num = 0;
    
    picParam->pic_init_qp = encCtx->enc.pic_init_qp_minus26 + QP_DEFAULT_VALUE;
    picParam->num_ref_idx_l0_active_minus1 = 0;
    picParam->num_ref_idx_l1_active_minus1 = 0;

    picParam->chroma_qp_index_offset = 0;
    picParam->second_chroma_qp_index_offset = 0;

    picParam->pic_fields.bits.idr_pic_flag = 0;
    picParam->pic_fields.bits.reference_pic_flag = 0;
    /* VAProfileH264ConstrainedBaseline only support ENTROPY_MODE_CAVLC, 
     * main/high can support ENTROPY_MODE_CAVLC and ENTROPY_MODE_CABAC 
     * when Undefined is selected, the behavior is profilespecific:CALV for Baseline, 
     * CABAC for Main and High 
     */
    if (encCtx->enc.profile == VAProfileH264ConstrainedBaseline) {
        picParam->pic_fields.bits.entropy_coding_mode_flag = ENTROPY_MODE_CAVLC;
    } else {
        picParam->pic_fields.bits.entropy_coding_mode_flag = ENTROPY_MODE_CABAC;
    }
    picParam->pic_fields.bits.weighted_pred_flag = 0;
    picParam->pic_fields.bits.weighted_bipred_idc = 0;
    
    
    if (encCtx->enc.constraint_set_flag & baselineOrMainlineMask) {
        picParam->pic_fields.bits.transform_8x8_mode_flag = 0;
    } else {
        picParam->pic_fields.bits.transform_8x8_mode_flag = 1;
    }

    picParam->pic_fields.bits.deblocking_filter_control_present_flag = 0; // 1;
    picParam->pic_fields.bits.pic_order_present_flag = 0;
    picParam->pic_fields.bits.pic_scaling_matrix_present_flag = 0;

    memset(picParam->ReferenceFrames, 0xFF, sizeof(picParam->ReferenceFrames)); /* invalid all */
}

/**
 * 功能说明：SEI table参数初始化，在CBR模式下会参考SEI参数
 */
static void EncContextSeiInit(avcenc_context_s *encCtx)
{
    if (!encCtx) {
        ENC_ERR_PRT("context null !!!\n");
        return;
    }
    
    int timeClock = 90000; // 时钟频率，9K
    int multiple = 2; // 此处是2倍的意思
    int initialCpbRemovalDelay = multiple * timeClock;
    int cpbRemovalDelay = 2;
    int initialCpbRemovalDelayLength = 24;
    int cpbRemovalDelayLength = 24;
    int dpbOutputDelayLength = 24;
    int timeOffsetLength = 24;
    /* it comes for the bps defined in SPS */
    encCtx->enc.i_initial_cpb_removal_delay = initialCpbRemovalDelay;
    encCtx->enc.i_initial_cpb_removal_delay_offset = initialCpbRemovalDelay;

    encCtx->enc.i_cpb_removal_delay = cpbRemovalDelay;
    encCtx->enc.i_initial_cpb_removal_delay_length = initialCpbRemovalDelayLength;
    encCtx->enc.i_cpb_removal_delay_length = cpbRemovalDelayLength;
    encCtx->enc.i_dpb_output_delay_length = dpbOutputDelayLength;
    encCtx->enc.time_offset_length = timeOffsetLength;

    encCtx->enc.prev_idr_cpb_removal = encCtx->enc.i_initial_cpb_removal_delay / timeClock;
    encCtx->enc.current_idr_cpb_removal = encCtx->enc.prev_idr_cpb_removal;
    encCtx->enc.current_cpb_removal = 0;
    encCtx->enc.idr_frame_num = 0;

    return;
}

/**
 * 功能说明：编码参数上下文初始化
 */
static void EncContextInit(avcenc_context_s *encCtx)
{
    if (!encCtx) {
        ENC_ERR_PRT("context null !!!\n");
        return;
    }
    int bit0 = 0;
    int bit1 = 1;
    int bit3 = 3;

    // 驱动目前支持BASELINE和MAIN两种方式
    switch (encCtx->enc.profile_idc) {
        case PROFILE_IDC_BASELINE:
            encCtx->enc.constraint_set_flag |= (1 << bit0); /* Annex A.2.1 */
            encCtx->enc.constraint_set_flag |= (1 << bit1); /* Annex A.2.2 */
            encCtx->enc.profile = VAProfileH264ConstrainedBaseline;
            break;
        case PROFILE_IDC_MAIN:
            encCtx->enc.constraint_set_flag |= (1 << bit1); /* Annex A.2.2 */
            encCtx->enc.profile = VAProfileH264Main;
            break;
        case PROFILE_IDC_HIGH:
            encCtx->enc.constraint_set_flag |= (1 << bit3); /* Annex A.2.4 */
            encCtx->enc.profile = VAProfileH264High;
            break;        
        default:
            break;
    }
    encCtx->enc.context_id = VA_INVALID_ID;
    encCtx->enc.config_id = VA_INVALID_ID;

    encCtx->enc.seq_param_buf_id = VA_INVALID_ID;
    encCtx->enc.pic_param_buf_id = VA_INVALID_ID;
    encCtx->enc.slice_param_buf_id = VA_INVALID_ID;
    encCtx->enc.codedbuf_buf_id = VA_INVALID_ID;
    encCtx->enc.misc_rate_control_buf_id = VA_INVALID_ID;
    encCtx->enc.misc_hrd_buf_id = VA_INVALID_ID;

    // 初始化编码数据的大小
    encCtx->enc.codedbuf_i_size = encCtx->enc.picture_width  * encCtx->enc.picture_height;
    encCtx->enc.codedbuf_pb_size = encCtx->enc.picture_width * encCtx->enc.picture_height;
    encCtx->enc.surface_idx = SID_INPUT_PICTURE_0;
 
    encCtx->enc.picture_width_in_mbs = (encCtx->enc.picture_width + SIZE_ALIGN - 1) / SIZE_ALIGN;
    encCtx->enc.picture_height_in_mbs = (encCtx->enc.picture_height + SIZE_ALIGN - 1) / SIZE_ALIGN;

    // 一帧大小是1.5*w*h
    encCtx->enc.frame_size = encCtx->enc.picture_width * encCtx->enc.picture_height +
                             ((encCtx->enc.picture_width * encCtx->enc.picture_height) >> 1) ;

    return;
}

/**
 * 功能说明：更新reference参考帧序列，进行排序
 */
static ENCStatus EncUpdateRefPicList(avcenc_context_s *encCtx)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    
    if (encCtx->enc.current_frame_type == SLICE_TYPE_P) {
        memcpy(encCtx->enc.RefPicList0, encCtx->enc.ReferenceFrames, encCtx->enc.numShortTerm * sizeof(VAPictureH264));
        SortOne(encCtx->enc.RefPicList0, 0, encCtx->enc.numShortTerm - 1, 0, 1); // 对帧序列从小到大排序
    }

    if (encCtx->enc.current_frame_type == SLICE_TYPE_B) {
        memcpy(encCtx->enc.RefPicList0,
               encCtx->enc.ReferenceFrames,
               encCtx->enc.numShortTerm * sizeof(VAPictureH264));
        SortTwo(encCtx->enc.RefPicList0, 0, encCtx->enc.numShortTerm - 1, encCtx->enc.current_poc, 0, 1, 0, 1);

        memcpy(encCtx->enc.RefPicList1, encCtx->enc.ReferenceFrames, encCtx->enc.numShortTerm * sizeof(VAPictureH264));
        SortTwo(encCtx->enc.RefPicList1, 0, encCtx->enc.numShortTerm - 1, encCtx->enc.current_poc, 0, 0, 1, 0);
    }

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：填充pps table
 * 输入参数：int sliceType 帧类型，I/P
 *           int isIdr 是否为IDR帧
 */
static ENCStatus EncUpdatePictureParam(avcenc_context_s *encCtx, int sliceType, int isIdr)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    VAEncPictureParameterBufferH264 *picParam;
    VAStatus vaStatus;
    int reconIndex;

    reconIndex = UtilityGetFreeSlot(encCtx);
    // Picture level
    picParam = &(encCtx->enc.pic_param);
    picParam->CurrPic.picture_id = encCtx->enc.ref_surface[reconIndex];
    picParam->CurrPic.frame_idx = encCtx->enc.current_frame_num;
    picParam->CurrPic.flags = 0;

    picParam->CurrPic.TopFieldOrderCnt = encCtx->enc.current_poc;
    picParam->CurrPic.BottomFieldOrderCnt = picParam->CurrPic.TopFieldOrderCnt;

    picParam->coded_buf = encCtx->enc.codedbuf_buf_id; // 指定对应的编码buffer
    picParam->frame_num = encCtx->enc.current_frame_num;
    picParam->pic_fields.bits.idr_pic_flag = !!isIdr;
    picParam->pic_fields.bits.reference_pic_flag = (sliceType != SLICE_TYPE_B);
    encCtx->enc.CurrentCurrPic = picParam->CurrPic;

    if (sliceType == SLICE_TYPE_P || sliceType == SLICE_TYPE_B) {
        memset(picParam->ReferenceFrames, 0xff, sizeof(picParam->ReferenceFrames));
    }

    if ((sliceType == SLICE_TYPE_P) || (sliceType == SLICE_TYPE_B)) {
        picParam->ReferenceFrames[0] = encCtx->enc.RefPicList0[0];
    }
    if (sliceType == SLICE_TYPE_B) {
        picParam->ReferenceFrames[1] = encCtx->enc.RefPicList1[0];
    }
    
    vaStatus = vaCreateBuffer(encCtx->va_dpy, encCtx->enc.context_id, VAEncPictureParameterBufferType,
                              sizeof(*picParam), 1, picParam, &(encCtx->enc.pic_param_buf_id));
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateBuffer", ENC_STATUS_ERROR_VA_STATUS);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：更新/填充slice级编码的参数
 * 输入参数：int sliceType 帧类型，I/P
 */
static ENCStatus EncUpdateSliceParam(avcenc_context_s *encCtx, int sliceType)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    // Slice level
    VAStatus vaStatus;
    int sliceAlphaC0OffsetDiv2 = 2;
    int sliceBetaOffsetDiv2 = 2;

    VAEncSliceParameterBufferH264 *slice_param = &(encCtx->enc.slice_param);
    memset(&(encCtx->enc.slice_param), 0, sizeof(encCtx->enc.slice_param));
    slice_param->macroblock_address = 0;
    slice_param->num_macroblocks = encCtx->enc.picture_height_in_mbs * encCtx->enc.picture_width_in_mbs;
    slice_param->pic_parameter_set_id = 0;
    slice_param->slice_type = sliceType;
    slice_param->direct_spatial_mv_pred_flag = 1;
    slice_param->num_ref_idx_l0_active_minus1 = 0;
    slice_param->num_ref_idx_l1_active_minus1 = 0;
    slice_param->cabac_init_idc = 0;
    slice_param->slice_qp_delta = 0;
    slice_param->disable_deblocking_filter_idc = 0;
    slice_param->slice_alpha_c0_offset_div2 = sliceAlphaC0OffsetDiv2;
    slice_param->slice_beta_offset_div2 = sliceBetaOffsetDiv2;
    slice_param->idr_pic_id = 0;
    slice_param->pic_order_cnt_lsb = 0;
    slice_param->direct_spatial_mv_pred_flag = 1;
    /* FIXME: fill other fields */
    if ((sliceType == SLICE_TYPE_P) || (sliceType == SLICE_TYPE_B)) {
        memset(slice_param->RefPicList0, 0xFF, sizeof(slice_param->RefPicList0));
        slice_param->RefPicList0[0] = encCtx->enc.RefPicList0[0];
    }

    if (sliceType == SLICE_TYPE_B) {
        memset(slice_param->RefPicList1, 0xFF, sizeof(slice_param->RefPicList1));
        slice_param->RefPicList1[0] = encCtx->enc.RefPicList1[0];
    }
     
    vaStatus = vaCreateBuffer(encCtx->va_dpy, encCtx->enc.context_id, VAEncSliceParameterBufferType,
                              sizeof(*slice_param), 1, slice_param, &(encCtx->enc.slice_param_buf_id));
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateBuffer", ENC_STATUS_ERROR_VA_STATUS);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明；更新reference参考帧序列
 */
static void EncUpdateRefFrames(avcenc_context_s *encCtx)
{
    int i;
    /* B-frame is not used for reference */
    if (encCtx->enc.current_frame_type == SLICE_TYPE_B) {
        return;
    }

    encCtx->enc.CurrentCurrPic.flags = VA_PICTURE_H264_SHORT_TERM_REFERENCE;
    encCtx->enc.numShortTerm++;
    if (encCtx->enc.numShortTerm > encCtx->enc.num_ref_frames) {
        encCtx->enc.numShortTerm = encCtx->enc.num_ref_frames;
    }
    for (i = encCtx->enc.numShortTerm - 1; i > 0; i--) {
        encCtx->enc.ReferenceFrames[i] = encCtx->enc.ReferenceFrames[i - 1];
    }
    encCtx->enc.ReferenceFrames[0] = encCtx->enc.CurrentCurrPic;

    encCtx->enc.current_frame_num++;
    if (encCtx->enc.current_frame_num > MAX_FRAME_NUM) {
        encCtx->enc.current_frame_num = 0;
    }

    /* Update the use_slot. Only when the surface is used in reference
     * frame list, the use_slot[index] is set
     */
    for (i = 0; i < SURFACE_NUM; i++) {
        bool found = false;
        for (unsigned int j = 0; j < encCtx->enc.numShortTerm; j++) {
            if (encCtx->enc.ref_surface[i] == encCtx->enc.ReferenceFrames[j].picture_id) {
                found = true;
                break;
            }
        }
        if (found) {
            encCtx->enc.use_slot[i] = 1;
        } else {
            encCtx->enc.use_slot[i] = 0;
        }
    }

    return;
}

/**
 * 功能说明：把编码后的h264数据从显存传输到内存
 * 输入参数：int sliceType 帧类型，I/P
 * 输出参数：avcenc_codedbuf_s *codedBuf 返回的编码数据
 */
static ENCStatus EncReceiveCodedBuffer(avcenc_context_s *encCtx, int sliceType, avcenc_codedbuf_s *codedBuf)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    VACodedBufferSegment *codedBufferSegment = NULL;
    VAStatus vaStatus;
    VASurfaceStatus surfaceStatus;

    struct timeval startTime = GetTimeOfDay();

    // 检查编码是否结束
    vaStatus = vaSyncSurface(encCtx->va_dpy, encCtx->enc.surface_ids[encCtx->enc.surface_idx]);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaSyncSurface", ENC_STATUS_ERROR_VA_STATUS);

    // 查询编码器编码状态，因为有几率存在编码不成功情况，即当创建的codedBuffer size小于实际编码的大小时，编码就会不成功
    // 当编码器状态为VA_CODED_BUF_STATUS_SLICE_OVERFLOW_MASK时，增大编码buffer size，重新编码此帧
    vaStatus = vaQuerySurfaceStatus(encCtx->va_dpy, encCtx->enc.surface_ids[encCtx->enc.surface_idx], &surfaceStatus);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaQuerySurfaceStatus", ENC_STATUS_ERROR_VA_STATUS);

    vaStatus = vaMapBuffer(encCtx->va_dpy, encCtx->enc.codedbuf_buf_id, (void **)(&codedBufferSegment));
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaMapBuffer", ENC_STATUS_ERROR_VA_STATUS);

    if (codedBufferSegment->status & VA_CODED_BUF_STATUS_SLICE_OVERFLOW_MASK) {
        if (sliceType == SLICE_TYPE_I) {
            encCtx->enc.codedbuf_i_size += encCtx->enc.codedbuf_i_size; // double size
        } else {
            encCtx->enc.codedbuf_pb_size += encCtx->enc.codedbuf_pb_size;
        }
        vaStatus = vaUnmapBuffer(encCtx->va_dpy, encCtx->enc.codedbuf_buf_id);
        VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaUnmapBuffer", ENC_STATUS_ERROR_VA_STATUS);

        DumpTimeuse(startTime, "        EncReceiveCodedBuffer", false, -1);
        return ENC_STATUS_ERROR_VA_SYNC_STATUS;
    }

    if (encCtx->enc.coded_h264_buffer) { // 把上一轮申请的buffer释放，后面再重新申请
        free(encCtx->enc.coded_h264_buffer);
        encCtx->enc.coded_h264_buffer = NULL;
    }
    encCtx->enc.coded_h264_buffer = malloc(codedBufferSegment->size); // 根据实际size申请空间
    memcpy(encCtx->enc.coded_h264_buffer, codedBufferSegment->buf, codedBufferSegment->size);
    codedBuf->coded_mem = encCtx->enc.coded_h264_buffer;
    codedBuf->coded_mem_size = codedBufferSegment->size;
    DumpH264Data(encCtx->enc.coded_h264_buffer, codedBufferSegment->size);

    // 数据拷贝出来后，把map的buffer释放掉
    vaStatus = vaUnmapBuffer(encCtx->va_dpy, encCtx->enc.codedbuf_buf_id);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaUnmapBuffer", ENC_STATUS_ERROR_VA_STATUS);

    DumpTimeuse(startTime, "        EncReceiveCodedBuffer", false, -1);
    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：删除创建的buffer
 */
static void EncBuffersDestory(avcenc_context_s *encCtx, VABufferID *vaBuffers, unsigned int vaBuffersNum)
{
    VAStatus vaStatus;

    for (unsigned int i = 0; i < vaBuffersNum; i++) {
        if (vaBuffers[i] != VA_INVALID_ID) {
            vaStatus = vaDestroyBuffer(encCtx->va_dpy, vaBuffers[i]);
            if (vaStatus != VA_STATUS_SUCCESS) {
                ENC_ERR_PRT("vaDestroyBuffer failed(%d)! num_va_buffers=%d\n", vaStatus, vaBuffersNum);
            }
            vaBuffers[i] = VA_INVALID_ID;
        }
    }

    return;
}

/**
 * 功能说明：填充misc rate ctrl,用于设置bitrate的max value and window size
 */
static ENCStatus EncUpdateMiscRateCtrl(avcenc_context_s *encCtx)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    if (encCtx->enc.bit_rate <= 0) { // VBR模式下，当bitrate大于0，需要设置maxbitrate和buffer_fullness
        return ENC_STATUS_SUCCESS;
    }

    // rate control parameter 
    // if rc mode is VBR, must set bits_per_second and target_percentage and buffer_size to vce
    VAStatus vaStatus;
    VAEncMiscParameterBuffer *miscRateCtrlBuffer;
    VAEncMiscParameterRateControl *miscRateCtrl;
    int percentage = 100;
    int thousand = 1000;
    
    // 创建一个空间，vaCreateBuffer是创建一块内存空间
    vaStatus = vaCreateBuffer(encCtx->va_dpy, encCtx->enc.context_id, VAEncMiscParameterBufferType,
                              sizeof(VAEncMiscParameterBuffer) + sizeof(VAEncMiscParameterRateControl),
                              1, NULL, &(encCtx->enc.misc_rate_control_buf_id));
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateBuffer", ENC_STATUS_ERROR_VA_STATUS);

    // 映射到用户空间，实际上没有做什么实际的动作
    vaStatus = vaMapBuffer(encCtx->va_dpy, encCtx->enc.misc_rate_control_buf_id, (void **)&miscRateCtrlBuffer);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaMapBuffer", ENC_STATUS_ERROR_VA_STATUS);

    // 更新rate ctrl参数到显存，vaUnmapBuffer是实际拷贝数据到显存的动作
    miscRateCtrlBuffer->type = VAEncMiscParameterTypeRateControl;
    miscRateCtrl = (VAEncMiscParameterRateControl *)miscRateCtrlBuffer->data;
    miscRateCtrl->bits_per_second = encCtx->enc.max_bit_rate;
    miscRateCtrl->target_percentage = (encCtx->enc.bit_rate * percentage) / encCtx->enc.max_bit_rate;
    miscRateCtrl->window_size = (encCtx->enc.max_bit_rate * thousand) / miscRateCtrl->bits_per_second;
    vaStatus = vaUnmapBuffer(encCtx->va_dpy, encCtx->enc.misc_rate_control_buf_id);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaUnmapBuffer", ENC_STATUS_ERROR_VA_STATUS);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：填充misc hrd,用于设置bitrate的buffer fullness
 */
static ENCStatus EncUpdateMiscHrd(avcenc_context_s *encCtx)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    if (encCtx->enc.bit_rate <= 0) { // VBR模式下，当bitrate大于0，需要设置maxbitrate和buffer_fullness
        return ENC_STATUS_SUCCESS;
    }
    /* hrd parameter */
    VAStatus vaStatus;
    VAEncMiscParameterBuffer *miscHRDBuffer;
    VAEncMiscParameterHRD *miscHRD;
    float bufferFullnessPercent = 0.75; // (float)3 / 4;
    vaStatus = vaCreateBuffer(encCtx->va_dpy, encCtx->enc.context_id, VAEncMiscParameterBufferType,
                              sizeof(VAEncMiscParameterBuffer) + sizeof(VAEncMiscParameterHRD),
                              1, NULL, &(encCtx->enc.misc_hrd_buf_id));
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateBuffer", ENC_STATUS_ERROR_VA_STATUS);

    // 映射存放hrd的显存到内存，把hrd的值更新进去
    vaStatus = vaMapBuffer(encCtx->va_dpy, encCtx->enc.misc_hrd_buf_id, (void **)&miscHRDBuffer);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaMapBuffer", ENC_STATUS_ERROR_VA_STATUS);

    miscHRDBuffer->type = VAEncMiscParameterTypeHRD;
    miscHRD = (VAEncMiscParameterHRD *)miscHRDBuffer->data;
    miscHRD->initial_buffer_fullness = (unsigned int)(encCtx->enc.max_bit_rate * bufferFullnessPercent);
    miscHRD->buffer_size = encCtx->enc.max_bit_rate; // hrd_buffer_size must >= max_bit_rate

    vaStatus = vaUnmapBuffer(encCtx->va_dpy, encCtx->enc.misc_hrd_buf_id);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaUnmapBuffer", ENC_STATUS_ERROR_VA_STATUS);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：填充sps table
 */
static ENCStatus EncUpdateSequenceParam(avcenc_context_s *encCtx)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);
   
    // sps table :sequence parameter set
    VAStatus vaStatus;
    VAEncSequenceParameterBufferH264 *seqParam = &(encCtx->enc.seq_param);
    vaStatus = vaCreateBuffer(encCtx->va_dpy, encCtx->enc.context_id, VAEncSequenceParameterBufferType,
                              sizeof(*seqParam), 1, seqParam, &(encCtx->enc.seq_param_buf_id));
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateBuffer", ENC_STATUS_ERROR_VA_STATUS);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：编码阶段，把YUV data/sps/pps/ratectrl/hrd/slice各buffer的值放入显存编码
 */
static ENCStatus EncEncodeStart(avcenc_context_s *encCtx)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    struct timeval startTime = GetTimeOfDay();
    
    VAStatus vaStatus;
    VABufferID vaBuffers[MAX_VA_BUFFERS];
    unsigned int vaBuffersNum = 0;

    vaBuffers[vaBuffersNum++] = encCtx->enc.seq_param_buf_id; // sps table
    vaBuffers[vaBuffersNum++] = encCtx->enc.pic_param_buf_id; // pps table

    if (encCtx->enc.misc_rate_control_buf_id != VA_INVALID_ID) {
        vaBuffers[vaBuffersNum++] = encCtx->enc.misc_rate_control_buf_id; // rate control buffer
    }

    if (encCtx->enc.misc_hrd_buf_id != VA_INVALID_ID) {
        vaBuffers[vaBuffersNum++] = encCtx->enc.misc_hrd_buf_id; // hrd buffer
    }

    // YUV数据 surface绑定到编码器context_id
    vaStatus = vaBeginPicture(encCtx->va_dpy, encCtx->enc.context_id, encCtx->enc.surface_ids[encCtx->enc.surface_idx]);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaBeginPicture", ENC_STATUS_ERROR_VA_STATUS);

    // sps/pps table参数，以及rateCtrl/hrd编码参数绑定到编码器context_id
    vaStatus = vaRenderPicture(encCtx->va_dpy, encCtx->enc.context_id, vaBuffers, vaBuffersNum);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaRenderPicture", ENC_STATUS_ERROR_VA_STATUS);

    // slice 宏快编码参数绑定到context_id
    vaStatus = vaRenderPicture(encCtx->va_dpy, encCtx->enc.context_id, &(encCtx->enc.slice_param_buf_id), 1);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaRenderPicture", ENC_STATUS_ERROR_VA_STATUS);

    // 把vaBeginPicture/vaRenderPicture中绑定的buffer内容拷贝到显存空间，并开始编码，编码模式为同步模式
    vaStatus = vaEndPicture(encCtx->va_dpy, encCtx->enc.context_id);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaEndPicture", ENC_STATUS_ERROR_VA_STATUS);

    DumpTimeuse(startTime, "        EncPictureStart", false, -1);
    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：创建存放编码后的显存
 * 输入参数：int sliceType 帧类型 I/P/B帧
 */
static ENCStatus EncCreateCodedBuffer(avcenc_context_s *encCtx, int sliceType)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    VAStatus vaStatus;
    int codedbufSize = (sliceType == SLICE_TYPE_I) ? encCtx->enc.codedbuf_i_size : encCtx->enc.codedbuf_pb_size;

    // 创建编码显存，即编码后的H264数据存放的位置
    vaStatus = vaCreateBuffer(encCtx->va_dpy, encCtx->enc.context_id, VAEncCodedBufferType, codedbufSize, 1, NULL,
                              &(encCtx->enc.codedbuf_buf_id));
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateBuffer", ENC_STATUS_ERROR_VA_STATUS);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：一帧编码结束，释放资源
 */
static void EncEncodeFinish(avcenc_context_s *encCtx)
{
    EncBuffersDestory(encCtx, &(encCtx->enc.seq_param_buf_id), 1); // EncUpdateSequenceParam
    EncBuffersDestory(encCtx, &(encCtx->enc.misc_rate_control_buf_id), 1); // EncUpdateMiscRateCtrl
    EncBuffersDestory(encCtx, &(encCtx->enc.misc_hrd_buf_id), 1); // EncUpdateMiscHrd
    EncBuffersDestory(encCtx, &(encCtx->enc.codedbuf_buf_id), 1);
    EncBuffersDestory(encCtx, &(encCtx->enc.pic_param_buf_id), 1); // EncUpdatePictureParam
    EncBuffersDestory(encCtx, &(encCtx->enc.slice_param_buf_id), 1); // EncUpdateSliceParam
    return;
}

/**
 * 功能说明：编码阶段
 *           1.更新SPS参数
 *           2.更新Rate Ctrl和Hrd,即帧率和比特率参数
 *           3.创建存放编码数据的显存
 *           4.更新参考帧序列
 *           5.更新PPS
 *           6.更新slice
 *           7.编码
 *           8.接收编码后的H264数据
 *           9.更新参考帧
 */
static ENCStatus EncPictureEncode(avcenc_context_s *encCtx, int isIdr, int sliceType, avcenc_codedbuf_s *codedBuf)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);

    ENCStatus encStatus;

    // 更新SPS编码参数
    encStatus = EncUpdateSequenceParam(encCtx);
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncUpdateSequenceParam", encStatus);

    // 在VBR模式下，bitrate大于0，设置bitrate的最大窗口值
    encStatus = EncUpdateMiscRateCtrl(encCtx);
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncUpdateMiscRateCtrl", encStatus);

    // 在VBR模式下，bitrate大于0，设置bitrate的最大buffer值
    encStatus = EncUpdateMiscHrd(encCtx);
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncUpdateMiscHrd", encStatus);

    do {
        // 因为codedBuffer size太小，申请的CodedBuffer无法存放编码后的数据，需要重新创建编码显存和编码
        // 一次编码没有成功，需要释放资源，更新大小后重新申请资源编码
        EncBuffersDestory(encCtx, &(encCtx->enc.codedbuf_buf_id), 1);
        EncBuffersDestory(encCtx, &(encCtx->enc.pic_param_buf_id), 1);
        EncBuffersDestory(encCtx, &(encCtx->enc.slice_param_buf_id), 1);

        // 创建一块存放编码数据的显存
        encStatus = EncCreateCodedBuffer(encCtx, sliceType);
        ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncCreateCodedBuffer", encStatus);

        // Update the RefPicList
        encStatus = EncUpdateRefPicList(encCtx);
        ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncUpdateRefPicList", encStatus);

        // pps: picture parameter set，更新PPS参数
        encStatus = EncUpdatePictureParam(encCtx, sliceType, isIdr);
        ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncUpdatePictureParam", encStatus);

        // slice parameter
        encStatus = EncUpdateSliceParam(encCtx, sliceType);
        ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncUpdateSliceParam", encStatus);

        // 编码开始
        encStatus = EncEncodeStart(encCtx);
        ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncEncodeStart", encStatus);

        // 编码结束，接收编码后的H264数据
        encStatus = EncReceiveCodedBuffer(encCtx, sliceType, codedBuf);

    } while (encStatus == ENC_STATUS_ERROR_VA_SYNC_STATUS);

    EncUpdateRefFrames(encCtx);

    // 释放编码资源
    EncEncodeFinish(encCtx);

    return ENC_STATUS_SUCCESS;
}

/**
 * 输入帧序号转换成显示顺序号
 * It is from the h264encode.c but it simplifies something.
 * For example: When one frame is encoded as I-frame under the scenario with
 * P-B frames, it will be regarded as IDR frame(key-frame) and then new GOP is
 * started. If the video clip is encoded as all I-frames, the first frame
 * is regarded as IDR and the remaining is regarded as I-frame.
 */
static void EncEncodeToDisplayOrder(unsigned long long encodingOrder, unsigned int gopSize, unsigned int ipPeriod,
                                    unsigned long long *displayOrder, int *frameType)
{
    /* When ip_period is 0, all are I/IDR frames */
    if (ipPeriod == 0) { /* all are I/IDR frames */
        if (encodingOrder == 0) {
            *frameType = FRAME_IDR;
        } else {
            *frameType = SLICE_TYPE_I;
        }

        *displayOrder = encodingOrder;
        return;
    }

    /* ipPeriod =1 IP; ipPeriod =2 IPB, but we cannot support B Frame
     * new sequence like
     * IDR PPPPP IDRPPPPP
     * IDR (PBB)(PBB)(PBB)(PBB) IDR (PBB)(PBB)(PBB)(PBB)
     */
    unsigned int encodingOrderGop = encodingOrder % gopSize;

    if (encodingOrderGop == 0) { /* the first frame */
        *frameType = FRAME_IDR;
        *displayOrder = encodingOrder;
    } else {
        unsigned int gopDelta = 1;

        // if support B Frame, please add process codes here that code has removed

        if (((encodingOrderGop - gopDelta) % ipPeriod) == 0) { /* P frames */
            *frameType = SLICE_TYPE_P;
            *displayOrder = encodingOrder + ipPeriod - 1;
        } else {
            *frameType = SLICE_TYPE_B;
            *displayOrder = encodingOrder - 1;
        }
    }

    return;
}

/**
 * 功能说明：CBR模式下更新IDR帧的RemoveTime
 */
static void EncUpdateIDRRemovalTime(avcenc_context_s *encCtx)
{
    int cpbRemovalDelay = 2;
    int currentDpbRemovalDelta = 2;
    unsigned long long frameInterval;

    /* Based on the H264 spec the removal time of the IDR access
    * unit is derived as the following:
    * the removal time of previous IDR unit + Tc * cpb_removal_delay(n)
    */
    frameInterval = encCtx->enc.enc_frame_number - encCtx->enc.idr_frame_num;
    encCtx->enc.current_cpb_removal = encCtx->enc.prev_idr_cpb_removal + frameInterval * cpbRemovalDelay;
    encCtx->enc.idr_frame_num = encCtx->enc.enc_frame_number;
    encCtx->enc.current_idr_cpb_removal = encCtx->enc.current_cpb_removal;
    if (encCtx->enc.ip_period) {
        encCtx->enc.current_dpb_removal_delta = (encCtx->enc.ip_period + 1) * currentDpbRemovalDelta;
    } else {
        encCtx->enc.current_dpb_removal_delta = currentDpbRemovalDelta;
    }
}

/**
 * 功能说明：CBR模式下更新非IDR帧的RemoveTime
 */
static void EncUpdateRemovalTime(avcenc_context_s *encCtx)
{
    int cpbRemovalDelay = 2;
    int currentDpbRemovalDelta = 2;
    unsigned long long frameInterval;

    /* Based on the H264 spec the removal time of the non-IDR access
    * unit is derived as the following:
    * the removal time of current IDR unit + Tc * cpb_removal_delay(n)
    */
    frameInterval = encCtx->enc.enc_frame_number - encCtx->enc.idr_frame_num;
    encCtx->enc.current_cpb_removal = encCtx->enc.current_idr_cpb_removal + frameInterval * cpbRemovalDelay;
    if (encCtx->enc.current_frame_type == SLICE_TYPE_I || encCtx->enc.current_frame_type == SLICE_TYPE_P) {
        if (encCtx->enc.ip_period) {
            encCtx->enc.current_dpb_removal_delta = (encCtx->enc.ip_period + 1) * currentDpbRemovalDelta;
        } else {
            encCtx->enc.current_dpb_removal_delta = currentDpbRemovalDelta;
        }
    } else {
        encCtx->enc.current_dpb_removal_delta = currentDpbRemovalDelta;
    }
}

/**
 * 功能说明：YUV数据编码成h264帧
 * 输入参数：int frameIndex，帧序列号，递增
 * 输出参数：avcenc_codedbuf_s *codedBuf 编码后的H264数据返回参数
 */
static ENCStatus EncYuvToH264(avcenc_context_s *encCtx, int frameIndex, avcenc_codedbuf_s *codedBuf)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    ENCStatus encStatus;
    int sliceType;
    struct timeval startTime = GetTimeOfDay();

    // 根据帧类型，计算显示序列号
    encCtx->enc.enc_frame_number = frameIndex;
    EncEncodeToDisplayOrder(encCtx->enc.enc_frame_number, encCtx->enc.intra_period, encCtx->enc.ip_period,
                            &(encCtx->enc.current_frame_display), &(encCtx->enc.current_frame_type));

    if (encCtx->enc.current_frame_type == FRAME_IDR) {
        encCtx->enc.numShortTerm = 0;
        encCtx->enc.current_frame_num = 0;
        memset(encCtx->enc.use_slot, 0, sizeof(encCtx->enc.use_slot));
        encCtx->enc.current_IDR_display = encCtx->enc.current_frame_display;
        if (encCtx->enc.rate_control_method == VA_RC_CBR) {
            EncUpdateIDRRemovalTime(encCtx);
        }
    } else {
        if (encCtx->enc.rate_control_method == VA_RC_CBR) {
            EncUpdateRemovalTime(encCtx);
        }
    }

    // use the simple mechanism to calc the POC
    encCtx->enc.current_poc = (encCtx->enc.current_frame_display - encCtx->enc.current_IDR_display) << 1;
    sliceType = (encCtx->enc.current_frame_type == FRAME_IDR) ? SLICE_TYPE_I : encCtx->enc.current_frame_type;
    
    // 开始编码
    encStatus = EncPictureEncode(encCtx, (encCtx->enc.current_frame_type == FRAME_IDR) ? 1 : 0, sliceType, codedBuf);
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncPictureEncode", encStatus);

    if ((encCtx->enc.current_frame_type == FRAME_IDR) && (encCtx->enc.rate_control_method == VA_RC_CBR)) {
        // after one IDR frame is written, it needs to update the
        // prev_idr_cpb_removal for next IDR
        encCtx->enc.prev_idr_cpb_removal = encCtx->enc.current_idr_cpb_removal;
    }
    
    DumpTimeuse(startTime, "    EncYuvToH264", false, -1);
    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：把用户空间的YUV数据按照YUV的格式排列放入surfaceMapBuffer
 */
static void EncYuvCpyToSurface(unsigned int yuvWidth, unsigned int yuvHeight, unsigned long *yuvSrc,
                               unsigned char *surfaceDst, VAImage surfaceImage)
{
    unsigned char *ySrc, *uSrc, *vSrc, *yDst, *uDst, *vDst;
    unsigned int ySize = yuvWidth * yuvHeight;
    unsigned int uSize = (yuvWidth >> 1) * (yuvHeight >> 1); // u size占1/4
    unsigned int yPlaneOffset = 0;
    unsigned int uPlaneOffset = 1;
    unsigned int vPlaneOffset = 2;

    unsigned char *newImageBuffer = (unsigned char*)yuvSrc;

    ySrc = newImageBuffer;
    uSrc = newImageBuffer + ySize; /* UV offset for NV12 */
    vSrc = newImageBuffer + ySize + uSize;

    yDst = surfaceDst + surfaceImage.offsets[yPlaneOffset];
    uDst = surfaceDst + surfaceImage.offsets[uPlaneOffset]; /* UV offset for NV12 */
    vDst = surfaceDst + surfaceImage.offsets[vPlaneOffset];

    // Y plane
    for (int row = 0; row < surfaceImage.height; row++) {
        memcpy(yDst, ySrc, surfaceImage.width);
        yDst += surfaceImage.pitches[0];
        ySrc += yuvWidth;
    }

    // UV plane
    if (surfaceImage.format.fourcc == VA_FOURCC_NV12) {
        // VA_FOURCC_NV12 格式存放顺序为 YYYYYY UVUVUVUV
        for (int row = 0; row < (surfaceImage.height >> 1); row++) {
            for (int col = 0; col < (surfaceImage.width >> 1); col++) {
                int i = col << 1;
                uDst[i] = uSrc[col];
                i++;
                uDst[i] = vSrc[col];
            }

            uDst += surfaceImage.pitches[1];
            uSrc += (yuvWidth >> 1);
            vSrc += (yuvWidth >> 1);
        }
    } else if (surfaceImage.format.fourcc == VA_FOURCC_YV12 || surfaceImage.format.fourcc == VA_FOURCC_I420) {
        // VA_FOURCC_YV12 格式存放顺序为：YYYYYY VVVVVVVVV UUUUUUUU
        // VA_FOURCC_I420 格式存放顺序为：YYYYYY UUUUUUUU VVVVVVVVV
        const int U = surfaceImage.format.fourcc == VA_FOURCC_I420 ? uPlaneOffset : vPlaneOffset;
        const int V = surfaceImage.format.fourcc == VA_FOURCC_I420 ? vPlaneOffset : uPlaneOffset;

        uDst = surfaceDst + surfaceImage.offsets[U];
        vDst = surfaceDst + surfaceImage.offsets[V];

        for (int row = 0; row < (surfaceImage.height >> 1); row++) {
            memcpy(uDst, uSrc, surfaceImage.width >> 1);
            memcpy(vDst, vSrc, surfaceImage.width >> 1);
            uDst += surfaceImage.pitches[U];
            vDst += surfaceImage.pitches[V];
            uSrc += (yuvWidth >> 1);
            vSrc += (yuvWidth >> 1);
        }
    }
    return;
}

/**
 * 功能说明；把内存空间的YUV数据拷贝到显存空间
 */
static ENCStatus EncUploadYuvToSurface(avcenc_context_s *encCtx, unsigned long *srcBuffer)
{
    CTX_RETURN_FAIL_IF(srcBuffer == NULL, "invalid param", ENC_STATUS_ERROR_INVALID_PARAMETER);

    struct timeval startTime = GetTimeOfDay();

    VAImage surfaceImage;
    void *surfaceMapBuffer = NULL;

    // 创建一块空间，image
    VAImageFormat imageFormat;
    imageFormat.fourcc = encCtx->fourcc;
    VAStatus vaStatus = vaCreateImage(encCtx->va_dpy, &imageFormat,
                                      encCtx->enc.picture_width, encCtx->enc.picture_height, &surfaceImage);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateImage", ENC_STATUS_ERROR_VA_STATUS);

    // 把image映射到用户空间surfaceMapBuffer
    vaStatus = vaMapBuffer(encCtx->va_dpy, surfaceImage.buf, &surfaceMapBuffer);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaMapBuffer", ENC_STATUS_ERROR_VA_STATUS);

    // 把YUV数据按照YUV的格式排列放入surfaceMapBuffer
    EncYuvCpyToSurface(encCtx->enc.picture_width, encCtx->enc.picture_height, srcBuffer,
                       (unsigned char *)surfaceMapBuffer, surfaceImage);

    vaStatus = vaUnmapBuffer(encCtx->va_dpy, surfaceImage.buf);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaUnmapBuffer", ENC_STATUS_ERROR_VA_STATUS);

    // 把内存空间的数据拷贝到显存enc.surface_ids，耗时步骤
    vaStatus = vaPutImage(encCtx->va_dpy, encCtx->enc.surface_ids[encCtx->enc.surface_idx], surfaceImage.image_id, 0, 0,
                          encCtx->enc.picture_width, encCtx->enc.picture_height,
                          0, 0, encCtx->enc.picture_width, encCtx->enc.picture_height);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaPutImage", ENC_STATUS_ERROR_VA_STATUS);

    // 释放资源
    vaStatus = vaDestroyImage(encCtx->va_dpy, surfaceImage.image_id);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaDestroyImage", ENC_STATUS_ERROR_VA_STATUS);

    DumpTimeuse(startTime, "yuvtosurface_timeuse", false, -1);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：把YUV数据拷贝到输出参数
 * 输出参数：void *bufSrc 待拷贝的数据
 *           avcenc_codedbuf_s *codedBuf 返回转换后的YUV数据，包括YUV的数据指针和数据大小
 */
static void VppYuvCpytoBuf(avcenc_context_s *encCtx, void *bufSrc, avcenc_codedbuf_s *codedBuf)
{
    // 每次都把上一次申请的资源释放掉后，再重新申请
    // 遗漏问题：可以改成只在开始申请一次，此处有内存泄漏
    unsigned int yuvFrameSize;
    if (encCtx->vpp.yuv_buffer) {
        free(encCtx->vpp.yuv_buffer);
        encCtx->vpp.yuv_buffer = NULL;
    }
    yuvFrameSize = (unsigned int)(encCtx->enc.picture_width * encCtx->enc.picture_height * YUV_BYTE_ONE_PIXEL);
    encCtx->vpp.yuv_buffer = malloc(yuvFrameSize);
    if (!encCtx->vpp.yuv_buffer) {
        ENC_ERR_PRT("yuv_buffer malloc failed! \n");
        return;
    }
    memcpy(encCtx->vpp.yuv_buffer, bufSrc, yuvFrameSize); // 把map出来的数据拷贝出来
    codedBuf->coded_mem = encCtx->vpp.yuv_buffer;
    codedBuf->coded_mem_size = yuvFrameSize;

    DumpYUVData(encCtx->vpp.yuv_buffer, yuvFrameSize); // YUV数据dump到本地文件中，此处为debug使用
    return;
}

/**
 * 功能说明：把YUV数据从显存传到内存空间
 * 输出参数：avcenc_codedbuf_s *codedBuf 返回转换后的YUV数据，包括YUV的数据指针和数据大小
 */
static ENCStatus VppReceiveYuvBuffer(avcenc_context_s *encCtx, avcenc_codedbuf_s *codedBuf)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    struct timeval startTime = GetTimeOfDay();

    VAStatus vaStatus;
    VAImage outImage;
    void *pBuf = NULL;
    VAImageFormat imageFormat;
    memset(&imageFormat, 0, sizeof(imageFormat));
    imageFormat.fourcc = encCtx->download_yuv_fourcc; // 指定YUV的数据格式，NV12/I420/YV12
    
    // 检查RGB转YUV流程是否结束，此处是判断GPU转换YUV是否完成的关键动作
    vaStatus = vaSyncSurface(encCtx->va_dpy, encCtx->enc.surface_ids[encCtx->enc.surface_idx]);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaSyncSurface", ENC_STATUS_ERROR_VA_STATUS);

    // 创建一块内存空间的image，用于存放从显存拷贝过来的YUV数据
    vaStatus = vaCreateImage(encCtx->va_dpy, &imageFormat,
                             encCtx->enc.picture_width, encCtx->enc.picture_height, &outImage);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateImage", ENC_STATUS_ERROR_VA_STATUS);

    // 把YUV数据从显存空间(enc.surface_ids)拷贝的内存空间(outImage)，此步骤是拷贝YUV数据耗时的一步
    vaStatus = vaGetImage(encCtx->va_dpy, encCtx->enc.surface_ids[encCtx->enc.surface_idx],
                          0, 0, encCtx->enc.picture_width, encCtx->enc.picture_height, outImage.image_id);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaGetImage", ENC_STATUS_ERROR_VA_STATUS);

    // 把内存空间的YUV数据映射到用户空间，outImage是已经在CPU内存空间了
    vaStatus = vaMapBuffer(encCtx->va_dpy, outImage.buf, &pBuf);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaMapBuffer", ENC_STATUS_ERROR_VA_STATUS);

    VppYuvCpytoBuf(encCtx, pBuf, codedBuf);

    // 释放资源
    vaStatus = vaUnmapBuffer(encCtx->va_dpy, outImage.buf);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaUnmapBuffer", ENC_STATUS_ERROR_VA_STATUS);
    
    // 删除资源
    vaStatus = vaDestroyImage(encCtx->va_dpy, outImage.image_id);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaDestroyImage", ENC_STATUS_ERROR_VA_STATUS);

    DumpTimeuse(startTime, "    DumpYUVData", false, -1);
    
    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：RGB数据转换成YUV数据
 */
ENCStatus VppRgbToYuv(avcenc_context_s *encCtx)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);
    
    ENCStatus encStatus = DumpRGBData(encCtx); // 保存RGB数据到用户空间
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "DumpRGBData", ENC_STATUS_SUCCESS);
    
    struct timeval startTime = GetTimeOfDay();
    
    VAStatus vaStatus;
    VAProcPipelineParameterBuffer *pipelineParam;
    
    // 把待转换的RGB数据通过map/unmap方式指定到pipeline_buf
    vaStatus = vaMapBuffer(encCtx->va_dpy, encCtx->vpp.pipeline_buf, (void **) &pipelineParam);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaMapBuffer", ENC_STATUS_ERROR_VA_STATUS);

    memset(pipelineParam, 0, sizeof(*pipelineParam));
    pipelineParam->surface = encCtx->vpp.surface_id; // 存储RGB的surface_id
    pipelineParam->surface_color_standard = VAProcColorStandardNone;
    pipelineParam->output_background_color = 0xff000000;
    pipelineParam->output_color_standard = VAProcColorStandardNone;
    vaStatus = vaUnmapBuffer(encCtx->va_dpy, encCtx->vpp.pipeline_buf);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaUnmapBuffer", ENC_STATUS_ERROR_VA_STATUS);

    // 转换准备：指定输出的YUV surface
    vaStatus = vaBeginPicture(encCtx->va_dpy, encCtx->vpp.context_id, encCtx->enc.surface_ids[encCtx->enc.surface_idx]);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaBeginPicture", ENC_STATUS_ERROR_VA_STATUS);
    
    // 把待转换的RGB数据指定到vpp.context_id，trans rgb surface to yuv surface
    vaStatus = vaRenderPicture(encCtx->va_dpy, encCtx->vpp.context_id, &(encCtx->vpp.pipeline_buf), 1);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaRenderPicture", ENC_STATUS_ERROR_VA_STATUS);
    
    // 开始转换RGB数据到YUV
    vaStatus = vaEndPicture(encCtx->va_dpy, encCtx->vpp.context_id);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaEndPicture", ENC_STATUS_ERROR_VA_STATUS);

    DumpTimeuse(startTime, "    VppRgbToYuv", false, -1);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：GPU驱动初始化
 */
static ENCStatus EncVaInit(avcenc_context_s *encCtx)
{
    ENC_INF_PRT("\n");
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);
    
    struct timeval startTime = GetTimeOfDay();

    int majorVer, minorVer;
    VAStatus vaStatus;
    vaStatus = vaInitialize(encCtx->va_dpy, &majorVer, &minorVer);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaInitialize", ENC_STATUS_ERROR_VA_STATUS);

    DumpTimeuse(startTime, "vaInitialize", false, -1);
    
    return (vaStatus == VA_STATUS_SUCCESS) ? ENC_STATUS_SUCCESS : ENC_STATUS_ERROR_VA_STATUS;
}

/**
 * 功能说明：GPU驱动资源释放
 */
static ENCStatus EncVaUninit(avcenc_context_s *encCtx)
{
    ENC_INF_PRT("\n");
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    VAStatus vaStatus;
    vaStatus = vaTerminate(encCtx->va_dpy);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaTerminate", ENC_STATUS_ERROR_VA_STATUS);

    encCtx->va_dpy = NULL; // 驱动vaTerminate后，需要把va_dpy赋NULL，否则会变成野指针
    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：查询image格式支援度
 */
static ENCStatus EncVaQueryImageFormat(avcenc_context_s *encCtx)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    ENCStatus encStatus = ENC_STATUS_SUCCESS;
    VAStatus vaStatus;
    VAImageFormat *imageFormats = NULL;
    int numImageFormats;
    int i;
    /**
     * image format query, YV12 / NV12
     */
    numImageFormats = vaMaxNumImageFormats(encCtx->va_dpy);
    if (!numImageFormats) {
        ENC_ERR_PRT("vaMaxNumImageFormats not support! \n");
        return ENC_STATUS_ERROR_CONFIG;
    }

    imageFormats = (VAImageFormat *)malloc(numImageFormats * sizeof(VAImageFormat));
    if (!imageFormats) {
        ENC_ERR_PRT("malloc fail t! \n");
        return ENC_STATUS_ERROR_MALLOC;
    }

    vaStatus = vaQueryImageFormats(encCtx->va_dpy, imageFormats, &numImageFormats);
    if (vaStatus != VA_STATUS_SUCCESS) {
        ENC_ERR_PRT("vaQueryImageFormats failed! \n");
        encStatus = ENC_STATUS_ERROR_CONFIG;
        goto END;
    }

    ENC_INF_PRT("support image format fourcc:\n");
    for (i = 0; i < numImageFormats; i++)  {
        ENC_INF_PRT("%s  \n",  MapFourccToStr(imageFormats->fourcc));
    }

END:
    free(imageFormats);
    imageFormats = NULL;
    return encStatus;
}

/**
 * 功能说明：查询GPU设备支援的画质级别，并设置正确的值
 */
ENCStatus EncVaQueryConfigProfiles(avcenc_context_s *encCtx)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    ENCStatus encStatus = ENC_STATUS_SUCCESS;
    VAStatus vaStatus;
    VAProfile  *profiles = NULL;
    int numProfiles;
    int i;
    /**
     * profile query, VAProfileH264ConstrainedBaseline / VAProfileH264Main
     */
    numProfiles = vaMaxNumProfiles(encCtx->va_dpy);
    if (!numProfiles) {
        ENC_ERR_PRT("vaMaxNumProfiles failed! \n");
        return ENC_STATUS_ERROR_CONFIG;
    }

    profiles = (VAProfile *)malloc(numProfiles * sizeof(VAProfile));
    if (!profiles) {
        ENC_ERR_PRT("malloc failed! \n");
        return ENC_STATUS_ERROR_MALLOC;
    }

    vaStatus = vaQueryConfigProfiles(encCtx->va_dpy, profiles, &numProfiles);
    if (vaStatus != VA_STATUS_SUCCESS) {
        ENC_ERR_PRT("vaQueryConfigProfiles failed! \n");
        encStatus = ENC_STATUS_ERROR_CONFIG;
        goto END;
    }
    for (i = 0; i < numProfiles; i++) {
        if (profiles[i] == encCtx->enc.profile) {
            break;
        }
    }
    
    // 用户设置的slice entry point不在支持列表内，返回错误信息
    if (i >= numProfiles) {
        ENC_ERR_PRT("vaQueryConfigProfiles failed! \n");
        encStatus = ENC_STATUS_ERROR_CONFIG;
        goto END;
    }

END:
    free(profiles);
    profiles = NULL;
    return encStatus;
}

/**
 * 功能说明：查询GPU设备支援的编码能力
 */
static ENCStatus EncVaQueryConfigEntrypoints(avcenc_context_s *encCtx)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    ENCStatus encStatus = ENC_STATUS_SUCCESS;
    VAStatus vaStatus;
    VAEntrypoint *entrypoints = NULL;
    int numEntrypoints;
    int i;
    /**
     * entrypoints query, VAEntrypointEncSlice/VAEntrypointVideoProc
     */
    numEntrypoints = vaMaxNumEntrypoints(encCtx->va_dpy);
    if (!numEntrypoints) {
        ENC_ERR_PRT("vaMaxNumEntrypoints failed! \n");
        return ENC_STATUS_ERROR_CONFIG;
    }
    entrypoints = (VAEntrypoint*)malloc(numEntrypoints * sizeof(VAEntrypoint));
    if (!entrypoints) {
        ENC_ERR_PRT("malloc failed! \n");
        return ENC_STATUS_ERROR_MALLOC;
    }

    vaStatus = vaQueryConfigEntrypoints(encCtx->va_dpy, encCtx->enc.profile, entrypoints, &numEntrypoints);
    if (vaStatus != VA_STATUS_SUCCESS) {
        ENC_ERR_PRT("vaQueryConfigEntrypoints failed! \n");
        encStatus = ENC_STATUS_ERROR_CONFIG;
        goto END;
    }
    for (i = 0; i < numEntrypoints; i++) {
        if (entrypoints[i] == encCtx->enc.select_entrypoint) {
            break;
        }
    }
    
    // 用户设置的slice entry point不在支持列表内，返回错误信息
    if (i >= numEntrypoints) {
        ENC_ERR_PRT("vaQueryConfigEntrypoints failed! \n");
        encStatus = ENC_STATUS_ERROR_CONFIG;
        goto END;
    }

END:
    free(entrypoints);
    entrypoints = NULL;
    return encStatus;
}

/**
 * 功能说明：设置编码属性以及创建编码驱动上下文
 */
static ENCStatus EncVaCreateContext(avcenc_context_s *encCtx)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    int numAttrib = 2;
    VAConfigAttrib attribs[numAttrib];
    VAStatus vaStatus;
    /**
    * find out the format for the render target, VA_RT_FORMAT_YUV420
    * and rate control mode, CBR/VBR/CQR
    */
    attribs[0].type = VAConfigAttribRTFormat;
    attribs[1].type = VAConfigAttribRateControl;
    vaStatus = vaGetConfigAttributes(encCtx->va_dpy, encCtx->enc.profile, encCtx->enc.select_entrypoint,
                                     attribs, numAttrib);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaGetConfigAttributes", ENC_STATUS_ERROR_CONFIG);
    
    // 检查用户设置的format是否在支持列表内(VA_RT_FORMAT_YUV420)
    if ((attribs[0].value & encCtx->enc.rt_format) == 0)  {
        ENC_ERR_PRT("rt format not support ! \n");
        return ENC_STATUS_ERROR_CONFIG;
    }
    
    // 检查用户设置的rate control是否支持列表内(VBR/CBR)
    if ((attribs[1].value & encCtx->enc.rate_control_method) == 0) {
        ENC_ERR_PRT("rc control not support ! \n");
        return ENC_STATUS_ERROR_CONFIG;
    }
    attribs[0].value = encCtx->enc.rt_format; // set to desired RT format
    attribs[1].value = encCtx->enc.rate_control_method; // set to desired RC mode

    vaStatus = vaCreateConfig(encCtx->va_dpy, encCtx->enc.profile, encCtx->enc.select_entrypoint,
                              attribs, numAttrib, &(encCtx->enc.config_id));
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateConfig", ENC_STATUS_ERROR_VA_STATUS);

    /**
    * Create a context for this decode pipe 
    */
    vaStatus = vaCreateContext(encCtx->va_dpy, encCtx->enc.config_id,
                               encCtx->enc.picture_width, encCtx->enc.picture_height,
                               VA_PROGRESSIVE, 0, 0, &(encCtx->enc.context_id));
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateContext", ENC_STATUS_ERROR_VA_STATUS);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：创建yuv数据以及参考帧的显存空间
 */
static ENCStatus EncVaCreateSurfaces(avcenc_context_s *encCtx)
{
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "display handle is null", ENC_STATUS_ERROR_DISPLAY_HANDLE);

    VAStatus vaStatus;

    // 创建存储YUV数据的surface，创建1个
    int surfAttribsNum = 1;
    VASurfaceAttrib surfAttribs[surfAttribsNum];
    surfAttribs[0].flags = VA_SURFACE_ATTRIB_SETTABLE;
    surfAttribs[0].type = VASurfaceAttribPixelFormat;
    surfAttribs[0].value.type = VAGenericValueTypeInteger;
    surfAttribs[0].value.value.i = encCtx->fourcc;
    vaStatus = vaCreateSurfaces(encCtx->va_dpy, encCtx->enc.rt_format, 
                                encCtx->enc.picture_width, encCtx->enc.picture_height,
                                encCtx->enc.surface_ids, SID_NUMBER, surfAttribs, surfAttribsNum);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateSurfaces", ENC_STATUS_ERROR_VA_STATUS);

    // 创建参考帧的surface 
    vaStatus = vaCreateSurfaces(encCtx->va_dpy, encCtx->enc.rt_format,
                                encCtx->enc.picture_width, encCtx->enc.picture_height,
                                encCtx->enc.ref_surface, SURFACE_NUM, surfAttribs, surfAttribsNum);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateSurfaces", ENC_STATUS_ERROR_VA_STATUS);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：创建上下文，初始化变量
 * 输入参数：avcencode_config_s encConfig 编码参数配置
 *           avcencode_input_s encInput 数据输入和输出类型配置
 * 返回值：编码上下文指针，管理全局参数
 */
void *ContextCreate(avcencode_config_s encConfig, avcencode_input_s encInput)
{
    avcenc_context_s *encCtx = (avcenc_context_s*)malloc(sizeof(avcenc_context_s));
    CTX_RETURN_FAIL_IF(encCtx == NULL, "context malloc fail!!!", NULL);

    memset(encCtx, 0x00, sizeof(avcenc_context_s));

    ENC_INF_PRT("ctx=%p, bitRate=%d, frameRate=%d, gop=%d, RC=%d, sizeIn=%d*%d, sizeOut=%d*%d, inType=%s, outType=%s\n",
                encCtx, encConfig.bit_rate, encConfig.frame_rate, encConfig.gop_size, encConfig.rate_control,
                encConfig.width_input, encConfig.height_input, encConfig.width_out, encConfig.height_out, 
                (encInput.source_raw_type == SOURCE_RAW_BO) ? "BO" : "YUV",
                (encInput.output_data_type == OUTPUT_YUV) ? "YUV" : "H264");
    ENC_INF_PRT("drm_device_path=%d\n", encConfig.drm_device_path);

    memcpy(&encCtx->avcencode_input, &encInput, sizeof(encCtx->avcencode_input));

    encCtx->drm_device_path = encConfig.drm_device_path; // gpu设备路径
    /* 
     * RGB转换为YUV时，可以转成后用户设定的宽高；
     * 有些因为客户端手机硬件限制，只能处理32bit对齐的数据，如果输入的数据为720，将会有一条黑边，如果为736，则画面正常
     * 因此，在某些场景，如果输入时720*1280，输出需要变成736*1280；这部分在RGB转换YUV时可以转换成指定的大小
    */
    encCtx->vpp.width = encConfig.width_input; // RGB数据的帧宽度
    encCtx->vpp.height = encConfig.height_input; // RGB数据的帧高度

    encCtx->enc.picture_width = encConfig.width_out; // YUV/H264数据的帧宽度
    encCtx->enc.picture_height = encConfig.height_out; // YUV/H264数据的帧高度

    encCtx->enc.frame_rate = encConfig.frame_rate;
    encCtx->enc.bit_rate = encConfig.bit_rate;

    /* I/P frames */
    encCtx->enc.gop_size = encConfig.gop_size;
    encCtx->enc.intra_period = encCtx->enc.gop_size;
    encCtx->enc.ip_period = 1; // 0(I frames only)/1(I and P frames)，默认为I/P帧

    encCtx->enc.rate_control_method = VA_RC_VBR; // Rate control支持VBR和CBR两种
    if (encConfig.rate_control == ENC_RC_CBR) {
        encCtx->enc.rate_control_method = VA_RC_CBR;
    }

    /*
     * profile_idc, support baseline & main
     */
    encCtx->enc.profile_idc = PROFILE_IDC_BASELINE;
    if (encConfig.profile_idc == ENC_PROFILE_IDC_MAIN) {
        encCtx->enc.profile_idc = PROFILE_IDC_MAIN;
    }
    
    /* bit rate & rate control */
    encCtx->enc.max_bit_rate = (unsigned int)(encCtx->enc.bit_rate * MAX_BITRATE_MULTIPLE);
    encCtx->enc.pic_init_qp_minus26 = PIC_INIT_QP_MINUS26;

    encCtx->vpp.pixel_format = VA_FOURCC_BGRA; // 显存RGBA的数据类型为BGRA
    encCtx->fourcc = VA_FOURCC_NV12; // surface fourcc only support VA_FOURCC_NV12

    // image format can support VA_FOURCC_I420/VA_FOURCC_NV12/VA_FOURCC_YV12
    encCtx->download_yuv_fourcc = VA_FOURCC_NV12;
    encCtx->enc.rt_format = VA_RT_FORMAT_YUV420; // 支持的编码格式为YUV420

    /* low-power mode:VAEntrypointEncSliceLP */
    encCtx->enc.select_entrypoint = VAEntrypointEncSlice; // 编码类型
    encCtx->enc.num_ref_frames = REF_FRAMES_NUM;

    encCtx->enc.coded_h264_buffer = NULL;
    
    EncContextInit(encCtx); // 初始化编码变量
    EncContextSeqParamInit(encCtx); // 初始化SPS参数
    EncContextPicParamInit(encCtx); // 初始化PPS参数
    if (encCtx->enc.rate_control_method == VA_RC_CBR) {
        EncContextSeiInit(encCtx);
    }

    return (void*)encCtx;
}

/**
 * 功能说明：释放上下文资源
 */
ENCStatus ContextDestory(void *encCtxHandle)
{
    CTX_RETURN_FAIL_IF(encCtxHandle == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);

    avcenc_context_s *encCtx = (avcenc_context_s*)encCtxHandle;

    // 释放打开的文件资源
    DumpResourceFree();

    if (encCtx->enc.coded_h264_buffer) {
        free(encCtx->enc.coded_h264_buffer);
        encCtx->enc.coded_h264_buffer = NULL;
    }

    // 释放全局上下文handle
    free(encCtx);
    encCtx = NULL;
    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：创建编码器；打开显卡设备节点，初始化驱动
 */
ENCStatus EncodeCreate(void *encCtxHandle)
{
    ENC_INF_PRT("\n");
    CTX_RETURN_FAIL_IF(encCtxHandle == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);

    ENCStatus encStatus;
    avcenc_context_s *encCtx = (avcenc_context_s*)encCtxHandle;
    
    // 打开设备节点
    encStatus = DisplayDrmOpen(encCtx);
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "DisplayDrmOpen", encStatus);
    
    // 初始化GPU mesa驱动
    encStatus = EncVaInit(encCtx);
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncVaInit", encStatus);

    return encStatus;
}

/**
 * 功能说明：释放编码；释放编码器驱动资源，关闭显卡设备节点
 */
ENCStatus EncodeDestroy(void *encCtxHandle)
{
    ENC_INF_PRT("\n");
    CTX_RETURN_FAIL_IF(encCtxHandle == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    ENCStatus encStatus;
    avcenc_context_s *encCtx = (avcenc_context_s*)encCtxHandle;
    
    // uninit编码驱动
    encStatus = EncVaUninit(encCtx);
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncVaUninit", encStatus);
    
    // 关闭显卡设备节点
    encStatus = DisplayDrmClose(encCtx);
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "DisplayDrmClose", encStatus);

    return encStatus;
}

/**
 * 功能说明：打开编码器，创建资源
 */
ENCStatus EncodeOpen(void *encCtxHandle)
{
    CTX_RETURN_FAIL_IF(encCtxHandle == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    avcenc_context_s *encCtx = (avcenc_context_s*)encCtxHandle;
    
    // 查询支持的image格式
    ENCStatus encStatus = EncVaQueryImageFormat(encCtx);
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncVaQueryImageFormat", encStatus);
    
    // 查询GPU卡支持的profile,并检查用户用户设置是否在支持列表内
    encStatus = EncVaQueryConfigProfiles(encCtx);
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncVaQueryConfigProfiles", encStatus);

    // 查询GPU卡支持的entrypoints,并检查用户用户设置是否在支持列表内
    encStatus = EncVaQueryConfigEntrypoints(encCtx);
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncVaQueryConfigEntrypoints", encStatus);
    
    // 创建视频编码上下文
    encStatus = EncVaCreateContext(encCtx);
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncVaCreateContext", encStatus);
    
    // 创建存放YUV数据的surface空间
    encStatus = EncVaCreateSurfaces(encCtx);
    ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncVaCreateSurfaces", encStatus);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：停止编码，释放编码资源
 */
ENCStatus EncodeClose(void *encCtxHandle)
{
    ENC_INF_PRT("\n");
    avcenc_context_s *encCtx = (avcenc_context_s*)encCtxHandle;
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);

    VAStatus vaStatus;
    vaStatus = vaDestroySurfaces(encCtx->va_dpy, encCtx->enc.surface_ids, SID_NUMBER); // 释放YUV surface
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaDestroySurfaces", ENC_STATUS_ERROR_VA_STATUS);

    vaStatus = vaDestroySurfaces(encCtx->va_dpy, encCtx->enc.ref_surface, SURFACE_NUM); // 释放参考帧surface
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaDestroySurfaces", ENC_STATUS_ERROR_VA_STATUS);

    vaStatus = vaDestroyContext(encCtx->va_dpy, encCtx->enc.context_id); // 释放上下文
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaDestroyContext", ENC_STATUS_ERROR_VA_STATUS);

    vaStatus = vaDestroyConfig(encCtx->va_dpy, encCtx->enc.config_id); // 释放配置id
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaDestroyConfig", ENC_STATUS_ERROR_VA_STATUS);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：创建视频处理器资源
 */
ENCStatus VppOpen(void *encCtxHandle)
{
    ENC_INF_PRT("\n");
    avcenc_context_s *encCtx = (avcenc_context_s*)encCtxHandle;
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);

    VAStatus vaStatus;

    // 视频处理的类型为VAEntrypointVideoProc
    vaStatus = vaCreateConfig(encCtx->va_dpy, VAProfileNone, VAEntrypointVideoProc, NULL, 0, &(encCtx->vpp.config_id));
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateConfig", ENC_STATUS_ERROR_VA_STATUS);

    // create context for video post processing
    vaStatus = vaCreateContext(encCtx->va_dpy, encCtx->vpp.config_id, encCtx->vpp.width, encCtx->vpp.height,
                               0, NULL, 0, &(encCtx->vpp.context_id));
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateContext", ENC_STATUS_ERROR_VA_STATUS);

    vaStatus = vaCreateBuffer(encCtx->va_dpy, encCtx->vpp.context_id, VAProcPipelineParameterBufferType,
                              sizeof(VAProcPipelineParameterBuffer), 1, NULL, &(encCtx->vpp.pipeline_buf));
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateBuffer", ENC_STATUS_ERROR_VA_STATUS);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：关闭视频处理器，此为RGB转YUV的视频处理器；释放资源
 */
ENCStatus VppClose(void *encCtxHandle)
{
    ENC_INF_PRT("\n");
    avcenc_context_s *encCtx = (avcenc_context_s*)encCtxHandle;
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);

    VAStatus vaStatus;
    vaStatus = vaDestroyBuffer(encCtx->va_dpy, encCtx->vpp.pipeline_buf); 
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaDestroyBuffer", ENC_STATUS_ERROR_VA_STATUS);

    vaStatus = vaDestroyContext(encCtx->va_dpy, encCtx->vpp.context_id);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaDestroyContext", ENC_STATUS_ERROR_VA_STATUS);

    vaStatus = vaDestroyConfig(encCtx->va_dpy, encCtx->vpp.config_id);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaDestroyConfig", ENC_STATUS_ERROR_VA_STATUS);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：绑定RGB的显存地址到视频处理器中申请的一块显存surface上
 * 输入参数：unsigned long *boHandle 用户层通过OpenGL获取到的一帧RGB显存地址
 */
ENCStatus VppBoCreate(void *encCtxHandle, unsigned long *boHandle)
{
    avcenc_context_s *encCtx = (avcenc_context_s*)encCtxHandle;
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);

    if (encCtx->avcencode_input.source_raw_type == SOURCE_RAW_YUV_FILE) {
        return ENC_STATUS_SUCCESS;
    }
    if (!boHandle) {
        ENC_ERR_PRT("bo handle is null! \n");
        return ENC_STATUS_ERROR_INVALID_PARAMETER;
    }
    int i = 0;
    int attribsNum = 3;
    VASurfaceAttrib attribs[attribsNum];
    VASurfaceAttribExternalBuffers external;
    memset(&external, 0, sizeof(external));
    external.pixel_format = encCtx->vpp.pixel_format;
    external.width = encCtx->vpp.width;
    external.height = encCtx->vpp.height;
    external.data_size = external.width * external.height * RGB_BYTE_ONE_PIXEL; // RGB格式数据一个像素占用4byte
    external.num_planes = 1;
    external.pitches[0] = external.width * RGB_BYTE_ONE_PIXEL;
    external.buffers = boHandle; // 把显存空间的RGB数据指针赋给视频处理器的参数
    external.num_buffers = 1;

    attribs[i].flags = VA_SURFACE_ATTRIB_SETTABLE;
    attribs[i].type = VASurfaceAttribMemoryType;
    attribs[i].value.type = VAGenericValueTypeInteger;
    attribs[i].value.value.i = VA_SURFACE_ATTRIB_MEM_TYPE_KERNEL_DRM; // RGB数据类型为内核空间的地址
    i++;
    attribs[i].flags = VA_SURFACE_ATTRIB_SETTABLE;
    attribs[i].type = VASurfaceAttribExternalBufferDescriptor;
    attribs[i].value.type = VAGenericValueTypePointer;
    attribs[i].value.value.p = &external;
    i++;
    attribs[i].flags = VA_SURFACE_ATTRIB_SETTABLE;
    attribs[i].type = VASurfaceAttribPixelFormat;
    attribs[i].value.type = VAGenericValueTypeInteger;
    attribs[i].value.value.i = encCtx->fourcc; // YUV的数据类型VA_FOURCC_NV12

    // 创建存放RGB数据的vpp.surface_id
    VAStatus vaStatus = vaCreateSurfaces(encCtx->va_dpy, VA_RT_FORMAT_RGB32, encCtx->vpp.width, encCtx->vpp.height,
                                         &(encCtx->vpp.surface_id), 1, attribs, attribsNum);
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaCreateSurfaces", ENC_STATUS_ERROR_VA_STATUS);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：视频处理显存资源释放,因为每帧数据地址更新，因此每帧编码都需绑定和资源释放
 */
ENCStatus VppBoDestory(void *encCtxHandle)
{
    avcenc_context_s *encCtx = (avcenc_context_s*)encCtxHandle;
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);

    if (encCtx->avcencode_input.source_raw_type == SOURCE_RAW_YUV_FILE) { // 如果不是RGB显存数据，直接返回
        return ENC_STATUS_SUCCESS;
    }

    VAStatus vaStatus =  vaDestroySurfaces(encCtx->va_dpy, &(encCtx->vpp.surface_id), 1); // 从对应的surface id上释放
    VA_STATUS_RETURN_FAIL_IF(vaStatus, "vaDestroySurfaces", ENC_STATUS_ERROR_VA_STATUS);

    return ENC_STATUS_SUCCESS;
}

/**
 * 功能说明：编码一帧。根据输入源配置两种编码流程：
 *          1.输入源为SOURCE_RAW_BO，表示数据是显存RGB数据
 *          1.1首先将存储RGB数据的显存buffer绑定到视频处理器的上下文
 *          1.2将RGB数据转换成YUV格式
 *          1.3如果上层配置输出数据为YUV，即从显存空间把已转换的YUV数据拷贝到内存空间，并通过codedBuf返回给用户层
 *          1.4如果上层配置输出数据为H264，则在GPU端把YUV数据编码成H264数据，并把H264数据从GPU显存空间拷贝到内存空间，
 *             并通过codedBuf返回给用户层
 *          1.5显存释放
 *          2.输入源为SOURCE_YUV_FILE，表示数据是CPU端的YUV数据；输出数据只有H264
 *          2.1将CPU内存空间的一帧YUV数据拷贝到GPU显存空间
 *          2.2将YUV数据编码成H264数据，并通过codedBuf返回给用户层
 * 输入参数：
 *          int frameIndex 帧序号，递增
 *          unsigned long *srcBuffer 一帧数据地址，如果是RGB数据，则为从显存获取到RGB显存地址
 * 输出参数：avcenc_codedbuf_s *codedBuf 返回编码后的数据，包括数据指针和大小
 */
ENCStatus EncodeOneFrame(void *encCtxHandle, int frameIndex, unsigned long *srcBuffer, avcenc_codedbuf_s *codedBuf)
{
    avcenc_context_s *encCtx = (avcenc_context_s*)encCtxHandle;
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);
    CTX_RETURN_FAIL_IF(encCtx->va_dpy == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);

    struct timeval startTime = GetTimeOfDay(); // 编码一帧时间的开始
    ENCStatus encStatus;

    if (encCtx->avcencode_input.source_raw_type == SOURCE_RAW_BO) { // 1.数据源为显存上的RGB数据
        encStatus = VppBoCreate(encCtx, srcBuffer); // 1.1 把RGB的显存地址绑定到视频处理器的上下文上
        ENC_STATUS_RETURN_FAIL_IF(encStatus, "VppBoCreate", encStatus);

        encStatus = VppRgbToYuv(encCtx); // 1.2RGB转成YUV
        ENC_STATUS_RETURN_FAIL_IF(encStatus, "VppRgbToYuv", encStatus);

        if (encCtx->avcencode_input.output_data_type == OUTPUT_YUV) {
            encStatus = VppReceiveYuvBuffer(encCtx, codedBuf); // 1.3直接返回YUV数据
        } else {
            encStatus = EncYuvToH264(encCtx, frameIndex, codedBuf); // 1.4YUV编码成H264
        }

        encStatus = VppBoDestory(encCtx); // 1.5显存释放
    } else { // 2.数据源为内存空间的一帧YUV数据
        encStatus = EncUploadYuvToSurface(encCtx, srcBuffer); // 2.1将CPU内存空间的一帧YUV数据拷贝到GPU显存空间
        ENC_STATUS_RETURN_FAIL_IF(encStatus, "EncUploadYuvToSurface", encStatus);

        // 2.2将YUV数据编码成H264数据，并通过codedBuf返回给用户层
        encStatus = EncYuvToH264(encCtx, frameIndex, codedBuf);
    }

    DumpTimeuse(startTime, "frame_one", false, frameIndex); // 保存编码一帧的时间
    return encStatus;
}

/**
 * 功能说明：编码参数动态更新
 * 输入参数：avcencode_config_s enConfig 支持frame_rate和bit_rate两个参数更新 
 */
ENCStatus EncodeParamsUpdate(void *encCtxHandle, avcencode_config_s enConfig)
{
    ENC_ERR_PRT("\n");
    avcenc_context_s *encCtx = (avcenc_context_s*)encCtxHandle;
    CTX_RETURN_FAIL_IF(encCtx == NULL, "ctx is null", ENC_STATUS_ERROR_CONTEXT_HANDLE);

    /* bit rate & rate control */
    if (enConfig.frame_rate > 0) {
        encCtx->enc.frame_rate = enConfig.frame_rate;
        encCtx->enc.max_bit_rate = (unsigned int)(encCtx->enc.bit_rate * MAX_BITRATE_MULTIPLE);
    }
    if (enConfig.bit_rate > 0) {
        encCtx->enc.bit_rate = enConfig.bit_rate;
    }
    ENC_ERR_PRT("bit_rate=%d, max_bit_rate=%d\n", encCtx->enc.bit_rate, encCtx->enc.max_bit_rate);

    return ENC_STATUS_SUCCESS;
}
