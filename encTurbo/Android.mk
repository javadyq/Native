LOCAL_PATH := $(call my-dir)


include $(CLEAR_VARS)

LOCAL_MODULE := libencTurbo
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_TAGS := optional

IGNORED_WARNNING = \
	-Wincompatible-pointer-types \

LOCAL_CFLAGS += \
	$(IGNORED_WARNNING) \


LOCAL_SRC_FILES := \
	src/va_display_drm.c \
	src/va_encode_api.c \
	src/va_encode_impl.c \

LOCAL_SHARED_LIBRARIES := \
	libva \
	libva-drm \

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/include \
	$(TOP)/external/libva/va \
	$(TOP)/external/libva/va/drm \

include $(BUILD_SHARED_LIBRARY)


include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := encTurbo
LOCAL_PROPRIETARY_MODULE := true

LOCAL_SRC_FILES := \
	demo/main.c \
	
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/demo \
	$(TOP)/external/libva/va \
	$(TOP)/external/libva/va/drm \

LOCAL_SHARED_LIBRARIES := \
	libva \
	libva-drm \
	libencTurbo \

include $(BUILD_EXECUTABLE)

