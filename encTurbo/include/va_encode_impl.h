/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * encTurbo is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 *     http://license.coscl.org.cn/MulanPSL
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

#ifndef VA_ENCODE_IMPL
#define VA_ENCODE_IMPL

#include <stdio.h>
#include <va.h>
#include <va_encode_common.h>

#define MAX_FRAME_NUM               (1<<12)
#define LOG2_MAX_FRAME_NUM          4
#define LOG2_MAX_PIC_ORDER_CNT_LSB  4
#define BITSTREAM_ALLOCATE_STEPPING 4096
#define SID_INPUT_PICTURE_0         0
#define SID_NUMBER                  1
#define SURFACE_NUM                 16
#define PIC_INIT_QP_MINUS26         2 // SUCH AS : 28-26

#define MAX_VA_BUFFERS              20
#define REF_PIC_LIST_NUM            32
#define REF_FRAMES_NUM              2
#define MAX_BITRATE_MULTIPLE        1.2 // 定义最大的bitrate阈值，1.2倍，值可以增大的
#define QP_DEFAULT_VALUE            26 // 默认的一个QP值，一般为26
#define RGB_BYTE_ONE_PIXEL          4 // RGBA数据，一个像素占用4个byte
#define YUV_BYTE_ONE_PIXEL          1.5 // (float)3 / 2;，YUV数据，一个像素占1.5byte

#define SIZE_ALIGN                  16 // 16 byte对齐
#define MAX_ARRAY_SIZE              512 // 申请的临时字串
#define CPB_BR_NAL_FACTOR_BASE      1200
#define CPB_BR_NAL_FACTOR_HIGH      1500

// 信息打印，需注意Android环境是否能打印出来
#define ENC_INF_PRT(...)            printf("[encTurbo INF][%s:%d]", __func__, __LINE__); \ 
                                    printf(__VA_ARGS__);

// error信息打印
#define ENC_ERR_PRT(...)            printf("[encTurbo ERR][%s:%d]", __func__, __LINE__); \
                                    printf(__VA_ARGS__);

#define VA_STATUS_RETURN_FAIL_IF(va_status, func, ret)                          \
    if ((va_status) != VA_STATUS_SUCCESS) {                                     \
        ENC_ERR_PRT("%s failed\n", func);                                       \
        return (ret);                                                           \
    }                                                                           \

#define ENC_STATUS_RETURN_FAIL_IF(encStatus, func, ret)                         \
    if ((encStatus) != ENC_STATUS_SUCCESS) {                                    \
        ENC_ERR_PRT("%s failed\n", func);                                       \
        return (ret);                                                           \
    }                                                                           \

#define CTX_RETURN_FAIL_IF(cond, mesg, ret)                                     \
        if (cond) {                                                             \
            ENC_ERR_PRT("%s\n", mesg);                                          \
            return (ret);                                                       \
        }                                                                       \

enum NAL_REF_TYPE {
    NAL_REF_IDC_NONE = 0,
    NAL_REF_IDC_LOW = 1,
    NAL_REF_IDC_MEDIUM = 2,
    NAL_REF_IDC_HIGH = 3,
};

/**
 * 定义编码中各table 类型，符合H264编解码标准定义
 */
enum NAL_TYPE {
    NAL_NON_IDR = 1,
    NAL_IDR = 5,
    NAL_SEI = 6,
    NAL_SPS = 7,
    NAL_PPS = 8,
    NAL_DELIMITER = 9,
};

/**
 * 定义IDR/I/P/B帧类型值，符合标准定义
 */
enum SLICE_TYPE {
    SLICE_TYPE_P = 0,
    SLICE_TYPE_B = 1,
    SLICE_TYPE_I = 2,
    FRAME_IDR = 7,
};

enum ENTPROY_MODE_TYPE {
    ENTROPY_MODE_CAVLC = 0,
    ENTROPY_MODE_CABAC = 1,
};

/**
 * 定义profile 类型值，mesa驱动只支持BASELINE和MAIN两种
 */
enum PROFILE_IDC_TYPE {
    PROFILE_IDC_BASELINE = 66,
    PROFILE_IDC_MAIN = 77,
    PROFILE_IDC_HIGH = 100,
};

typedef struct {
    unsigned int *buffer;
    int bit_offset;
    int max_size_in_dword;
} bitstream;

typedef struct {
    DRMDevicePath drm_device_path;
    VADisplay va_dpy;
    int drm_fd;
    unsigned int fourcc; // YUV surface的数据格式
    unsigned int download_yuv_fourcc; // 接收到用户空间的YUV格式
    avcencode_input_s avcencode_input; // 数据输入输出类型定义

    /* video post processing is used for colorspace conversion */
    struct {
        VAConfigID config_id;
        VAContextID context_id;
        VABufferID pipeline_buf;
        VASurfaceID surface_id;
        int width; // RGBA格式数据宽度，此处RGBA数据w和enc里面的YUV/H264 w分开定义，是因为输入输出数据的大小可以不同
        int height; // RGBA格式数据高度
        unsigned int pixel_format; // RGBA数据格式，比如VA_FOURCC_BGRA
        void *yuv_buffer; // 存储转换后的YUV数据
        int reserved;
    } vpp;

    struct {
        VAEncSequenceParameterBufferH264 seq_param; // sps table数据结构
        VAEncPictureParameterBufferH264 pic_param; // pps table数据结构
        VAEncSliceParameterBufferH264 slice_param; // slice 宏块数据结构
        VAContextID context_id;
        VAConfigID config_id;
        VABufferID seq_param_buf_id; 
        VABufferID pic_param_buf_id;
        
        VABufferID slice_param_buf_id;
        VABufferID codedbuf_buf_id; // 编码后的buffer
        VABufferID misc_hrd_buf_id;
        VABufferID misc_rate_control_buf_id;
        
        int constraint_set_flag;
        int codedbuf_i_size;
        int codedbuf_pb_size;
        
        int surface_idx;
        int rate_control_method;

        int i_initial_cpb_removal_delay;
        int i_initial_cpb_removal_delay_offset;
        int i_initial_cpb_removal_delay_length;
        int i_cpb_removal_delay;
        int i_cpb_removal_delay_length;
        int i_dpb_output_delay_length;
        int time_offset_length;

        unsigned long long idr_frame_num;
        unsigned long long prev_idr_cpb_removal;
        unsigned long long current_idr_cpb_removal;
        unsigned long long current_cpb_removal;
        unsigned int current_dpb_removal_delta;

        VAPictureH264 ReferenceFrames[REF_FRAMES_NUM];
        VAPictureH264 RefPicList0[REF_PIC_LIST_NUM];
        VAPictureH264 RefPicList1[REF_PIC_LIST_NUM];
        VASurfaceID surface_ids[SID_NUMBER]; // YUV surface
        VASurfaceID ref_surface[SURFACE_NUM]; // 参考帧surface
        int use_slot[SURFACE_NUM];
        unsigned long long current_frame_display; 
        unsigned long long current_IDR_display;
        VAPictureH264 CurrentCurrPic;
        unsigned long long enc_frame_number;
        int current_frame_type;
        int current_frame_num;
        unsigned int current_poc;
        unsigned int num_ref_frames;
        unsigned int numShortTerm;

        int picture_width_in_mbs;
        int picture_height_in_mbs;
        int frame_size;
        VAEntrypoint select_entrypoint; // 编码类型为VAEntrypointEncSlice
        VAProfile profile; // base/main
        unsigned int rt_format; // VA_RT_FORMAT_YUV420
        int pic_init_qp_minus26;

        unsigned int intra_period; // I frame interval.
        unsigned int ip_period; // I/P frame interval.
        unsigned int bit_rate; // bps.
        unsigned int max_bit_rate; // bps.
        unsigned int frame_rate; // fps.
        unsigned int gop_size; // 图像组，两个I帧之间的距离

        unsigned int profile_idc;
        unsigned int picture_width; // 输出的YUV或者H264的图像宽度
        unsigned int picture_height; // 输出的YUV或者H264的图像高度

        void *coded_h264_buffer;
    }enc;

} avcenc_context_s;

void *ContextCreate(avcencode_config_s encConfig, avcencode_input_s encInput);
ENCStatus ContextDestory(void *encCtx);

ENCStatus EncodeCreate(void *encCtx);
ENCStatus EncodeDestroy(void *encCtx);

ENCStatus EncodeOpen(void *encCtx);
ENCStatus EncodeClose(void *encCtx);

ENCStatus VppOpen(void *encCtx);
ENCStatus VppClose(void *encCtx);

ENCStatus EncodeOneFrame(void *encCtx, int frameIndex, unsigned long *srcBuffer, avcenc_codedbuf_s *codedBuf);

ENCStatus EncodeParamsUpdate(void *encCtx, avcencode_config_s encConfig);

#endif
