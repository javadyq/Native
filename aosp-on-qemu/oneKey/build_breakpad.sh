name=`git config user.name`
if [ -z "$name" ]; then
  echo "please set git config user.name"
  exit
  fi
email=`git config user.email`
if [ -z "$email" ]; then
  echo "please set git config user.emial"
  exit
  fi


if [ x$1 == x ]; then
  echo "please input your emu project path"
  exit
  fi
EMU_PATH=$1

cd $EMU_PATH

if [ ! -d "$EMU_PATH/breakpad" ]; then
    git clone https://chromium.googlesource.com/breakpad/breakpad
    fi
cd breakpad
git clone  https://chromium.googlesource.com/linux-syscall-support src/third_party/lss
git reset --hard 1f1d950d6a4b1cb375ebfbc4e40f9517c592b94f
./configure --prefix=$EMU_PATH/prebuilts/android-emulator-build/common/breakpad/linux-aarch64
make && make install

cp $EMU_PATH/breakpad/src/third_party/libdisasm/libdisasm.a $EMU_PATH/prebuilts/android-emulator-build/common/breakpad/linux-aarch64/lib/
