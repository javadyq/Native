
if [ x$1 == x ]; then
  echo "please input your emu project path"
  exit
  fi
EMU_PATH=$1
ARCHIVE=$EMU_PATH/prebuilts/android-emulator-build/archive/
LOCAL_PREFIX=$EMU_PATH/prebuilts/android-emulator-build/archive/hw_build_protobuf
if [ ! -d "$LOCAL_PREFIX" ]; then
  mkdir $LOCAL_PREFIX
  fi

tar -xf $ARCHIVE/protobuf-v3.0.0.tar* -C $LOCAL_PREFIX

cd $LOCAL_PREFIX

if [ ! -d "$LOCAL_PREFIX/gtest" ]; then
  tar -xf $ARCHIVE/googletest-release-1.7.0.tar* -C $LOCAL_PREFIX
  mv googletest-release-1.7.0 gtest
  fi
if [ ! -d "$LOCAL_PREFIX/gmock" ]; then
  tar -xf $ARCHIVE/googlemock-release-1.7.0.tar* -C $LOCAL_PREFIX
  mv googlemock-release-1.7.0 gmock
  fi

cp -r gtest gmock/
cp -r gmock protobuf-v3.0.0/

cd protobuf-v3.0.0/

./autogen.sh
sed -i 's/  CFLAGS=""/  CFLAGS="-fPIC"/' configure
sed -i 's/  CXXFLAGS=""/  CXXFLAGS="-fPIC"/' configure

./configure --prefix=$EMU_PATH/prebuilts/android-emulator-build/protobuf/linux-aarch64 --disable-shared
make && make install
