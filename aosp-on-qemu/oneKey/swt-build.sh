# 创建目录
CODE_DIR=code_dir
SWT_DIR=swt_dir
LOCAL_PATH=`pwd`
mkdir $CODE_DIR
cd $CODE_DIR

# 下载源码
git clone http://git.eclipse.org/gitroot/platform/eclipse.platform.swt.git
cd eclipse.platform.swt
git reset --hard 0e6012bf1d43e344acbdce973bb61486db873c8f
cd ..

git clone http://git.eclipse.org/gitroot/platform/eclipse.platform.swt.binaries.git
cd eclipse.platform.swt.binaries
git reset --hard a147651a7855a0e5fc11277c2887e92dba8a39d6
cd $LOCAL_PATH

# 编译
cd $CODE_DIR/eclipse.platform.swt.binaries/bundles/org.eclipse.swt.gtk.linux.aarch64
ant

# swt.jar解压及打包
cd $LOCAL_PATH
mkdir $SWT_DIR
cp $CODE_DIR/eclipse.platform.swt.binaries/bundles/org.eclipse.swt.gtk.linux.aarch64/swt.jar $SWT_DIR
cp /usr/lib/jni/libswt* $SWT_DIR

# 编译环境ARM1616，java版本1.8.0
cd $SWT_DIR
ln -sf /usr/lib/jvm/java-8-openjdk-arm64/bin/jar /usr/bin/jar
jar -xvf swt.jar
rm -f swt.jar
jar cvfM0 swt.jar ./

#拷贝至指定目录
#cd $LOCAL_PATH
#mkdir -p android-sdk-linux/tools/lib/aarch64/
#cp $SWT_DIR/swt.jar android-sdk-linux/tools/lib/aarch64/
